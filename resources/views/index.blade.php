@php ($pgName = 'Hello Himalayn Homes')
	@include('includes.top')
	<div class="page-wrapper">
		<div class="header-wrapper">
			<div class="header">
				<div class="header-inner">
					@include('includes.nav')
				</div>
			</div>
		</div>
		<div class="main-wrapper">
			<div class="main">
				<div class="main-inner">
					<div class="content">
						<div id="hero-slider">
							<div class="hero">	
								<div class="hero-carousel">
									<div class="hero-carousel-item">
										<div class="hero-image" style="background-image: url('public/assets/img/gallery/1.jpg')"></div>
										<div class="hero-carousel-title">
											<div class="container">
												<div class="hero-carousel-title-inner">
													<h1>Complete Information of Hotels</h1>
													<h2>Bed, Breakfast, Restaurant</h2>
													<a href="#" class="btn btn-primary map-link">Search in Map</a>
												</div>
											</div>
										</div>	
									</div>
									<div class="hero-carousel-item">
										<div class="hero-image" style="background-image: url('public/assets/img/gallery/3.jpg')"></div>
										<div class="hero-carousel-title">
											<div class="container">
												<div class="hero-carousel-title-inner">
													<h1>Find perfect Climbing Guide </h1>
													<h2>for trekking and climbing</h2>
													<a href="#" class="btn btn-primary map-link">Search in Map</a>
												</div>
											</div>
										</div>	
									</div>
								</div>
								<div class="hero-form dark">
									<div class="container">
										<form method="get" action="?">
											<div class="input-group first col-sm-12 col-md-10">
												<div class="input-group-addon">
													<i class="md-icon">my_location</i>
												</div>
												<input id="keywords" autofocus type="text" name="q" class="form-control" placeholder="e.g. Khumbu">
											</div>
											<div class="form-group last col-sm-12 col-md-2">
												<button type="submit" class="btn btn-primary btn-block">Search</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div id="hero-map">
							<div class="hero">
								<div class="header-bottom">
									<div class="container">
										<div class="header-bottom-label hidden-md-down">
											I'm looking for
										</div>
										<ul class="nav nav-pills">
											<li class="nav-item"><a href="#" class="nav-link">Hotels</a></li>
											<li class="nav-item"><a href="#" class="nav-link">Lodges</a></li>
											<li class="nav-item"><a href="#" class="nav-link">Restaurants</a></li>
											<li class="nav-item"><a href="#" class="nav-link">Home Stay</a></li>
											<li class="nav-item"><a href="#" class="nav-link">Guides</a></li>
											<li class="nav-item"><a href="#" class="nav-link">Climbers</a></li>
										</ul>
									</div>
								</div>
								<div id="map-leaflet" class="full"></div>
							</div>
						</div>
						<div class="container">
							<div class="page-title">
								<h2><a href="list.blade.php">Featured Listings</a></h2>
							</div>
							<div class="row">
								<div class="col-sm-12 col-xl-10">
									<div class="cards-wrapper">
										<div class="row">
											<div class="col-sm-6 col-md-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/2.jpg');">
														<a href="#"></a> 
														<div class="card-image-rating">
															<i class="md-icon" title="Bed">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="#">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/3.jpg');">
														<a href="#"></a> 
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="#">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/4.jpg');">
														<a href="#"></a> 
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="#">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/5.jpg');">
														<a href="#"></a> 
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="#">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/6.jpg');">
														<a href="#"></a> 
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="#">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/7.jpg');">
														<a href="#"></a> 
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="#">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-2 hidden-lg-down">
									<div class="your-space">
										<p>Do you want to be here?</p>
										<a href="#" class="btn btn-primary btn-block">+1-254-689</a>
										<a href="#" class="btn btn-secondary btn-block">Contact</a>
									</div>
								</div>	
							</div>
						</div>
						<div class="container">
							<div class="page-title background-white">
								<h2><a href="list.blade.php">Featured Climbers</a></h2>
							</div>
							<div class="cards-wrapper">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-lg-3">
										<div class="card">
											<div class="card-image small no-line" style="background-image: url('public/assets/img/climbers/1.jpg');">
												<a href="detail1.php"></a>
												<div class="card-image-count">
													<strong>12</strong>
													<span>Climbs</span>
												</div>
											</div>
											<div class="card-content">
												<h2><a href="detail1.php">Ang Babu Sherpa</a></h2>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-lg-3">
										<div class="card">
											<div class="card-image small no-line" style="background-image: url('public/assets/img/climbers/1.jpg');">
												<a href="detail1.php"></a>
												<div class="card-image-count">
													<strong>12</strong>
													<span>Climbs</span>
												</div>
											</div>
											<div class="card-content">
												<h2><a href="detail1.php">Ang Babu Sherpa</a></h2>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-lg-3">
										<div class="card">
											<div class="card-image small no-line" style="background-image: url('public/assets/img/climbers/1.jpg');">
												<a href="detail1.php"></a>
												<div class="card-image-count">
													<strong>12</strong>
													<span>Climbs</span>
												</div>
											</div>
											<div class="card-content">
												<h2><a href="detail1.php">Ang Babu Sherpa</a></h2>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-lg-3">
										<div class="card">
											<div class="card-image small no-line" style="background-image: url('public/assets/img/climbers/1.jpg');">
												<a href="detail1.php"></a>
												<div class="card-image-count">
													<strong>12</strong>
													<span>Climbs</span>
												</div>
											</div>
											<div class="card-content">
												<h2><a href="detail1.php">Ang Babu Sherpa</a></h2>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--<div class="counter">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 col-md-3">
										<h2>Best directory listing of Nepal</h2>
									</div>
									<div class="col-sm-12 col-md-9">
										<div class="row">
											<div class="col-sm-3">
												<i class="fa fa-fw fa-hotel"></i>
												<h3>324</h3>
												<p>Hotels</p>
											</div>
											<div class="col-sm-3">
												<i class="fa fa-fw fa-home"></i> 
												<h3>593</h3>
												<p>Home Stay</p>
											</div>				
											<div class="col-sm-3">
												<i class="fa fa-fw fa-user"></i>
												<h3>1897</h3>
												<p>Guides</p>
											</div>	
											<div class="col-sm-3">
												<i class="fa fa-fw fa-area-chart"></i>
												<h3>408</h3>
												<p>Climbers</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>-->
						<div class="push-bottom">
							<div class="container">
								<div class="page-title">
									<h2><a href="list.blade.php">Recent News & Events</a></h2>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-4">
										<div class="card">
											<div class="card-image" style="background-image: url('public/assets/img/user/2.jpg');">
												<a href="#"></a>
											</div>
											<div class="card-content">
												<h3><a href="#">Marketing</a></h3>
												<h2><a href="#">Meeting All Deadlines</a></h2>
											</div>
											<div class="card-actions">
												<a href="#" class="card-action-icon"><i class="md-icon">insert_comment</i><span> 12</span></a>
												<a href="#" class="card-action-icon"><i class="md-icon">access_time</i><span> 18. June</span></a>
												<a href="#" class="card-action-btn btn btn-transparent">Read More</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-md-4">
										<div class="card">
											<div class="card-image" style="background-image: url('public/assets/img/user/2.jpg');">
												<a href="#"></a>
											</div>
											<div class="card-content">
												<h3><a href="#">Marketing</a></h3>
												<h2><a href="#">Meeting All Deadlines</a></h2>
											</div>
											<div class="card-actions">
												<a href="#" class="card-action-icon"><i class="md-icon">insert_comment</i><span> 12</span></a>
												<a href="#" class="card-action-icon"><i class="md-icon">access_time</i><span> 18. June</span></a>
												<a href="#" class="card-action-btn btn btn-transparent">Read More</a>
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-md-4">
										<div class="card">
											<div class="card-image" style="background-image: url('public/assets/img/user/2.jpg');">
												<a href="#"></a>
											</div>
											<div class="card-content">
												<h3><a href="#">Marketing</a></h3>
												<h2><a href="#">Meeting All Deadlines</a></h2>
											</div>
											<div class="card-actions">
												<a href="#" class="card-action-icon"><i class="md-icon">insert_comment</i><span> 12</span></a>
												<a href="#" class="card-action-icon"><i class="md-icon">access_time</i><span> 18. June</span></a>
												<a href="#" class="card-action-btn btn btn-transparent">Read More</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="partners">
							<div class="container">
								<a href="#">
									<img src="public/assets/img/partners/partner-1.png" alt="" height="74">
								</a>
								<a href="#">
									<img src="public/assets/img/partners/partner-1.png" alt="" height="74">
								</a>
								<a href="#">
									<img src="public/assets/img/partners/partner-1.png" alt="" height="74">
								</a>
								<a href="#">
									<img src="public/assets/img/partners/partner-1.png" alt="" height="74">
								</a>
								<a href="#">
									<img src="public/assets/img/partners/partner-1.png" alt="" height="74">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('includes.footer')
	</div>
	<a href="javascript:void(0);" id="scroll" title="Scroll to Top" style="display: none;">Top<span></span></a>
	<div class="customizer">
		<div class="customizer-content">
			<h2>Get Social</h2>
			<ul>
				<li><a href="#"><i class="fa fa-fw fa-facebook"></i></a></li>
				<li><a href="#"><i class="fa fa-fw fa-twitter"></i></a></li>
				<li><a href="#"><i class="fa fa-fw fa-youtube"></i></a></li>
				<li><a href="#"><i class="fa fa-fw fa-google-plus"></i></a></li>
			</ul> 
		</div>
		<div class="customizer-title">
			<span><i class="fa fa-cog"></i> Get Social</span>
		</div>
	</div>
@include('includes.btm')