	<?php ($pgName = 'List | Hello Himalayn Homes'); ?>
	<?php echo $__env->make('includes.top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="page-wrapper">
		<div class="header-wrapper">
			<div class="header">
				<div class="header-inner">
					<?php echo $__env->make('includes/nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<?php echo $__env->make('includes/nav1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
			</div>
		</div>
		<div class="main-wrapper">
			<div class="main">
				<div class="main-inner">
					<div class="content-title">
						<div class="container">
							<h1>Listings</h1>
							<ul class="breadcrumb">
								<li><a href="<?php echo e(url('/')); ?>">Home</a> <i class="md-icon">keyboard_arrow_right</i></li>
								<li class="active">Listings</li>
							</ul>
						</div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-lg-9">
								<div class="content">
									<div class="push-top-bottom">
										<div class="row">
											<div class="col-sm-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/3.jpg');">
														<a href="<?php echo e(url('hotel-details')); ?>"></a>
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="<?php echo e(url('hotel-details')); ?>">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/3.jpg');">
														<a href="<?php echo e(url('hotel-details')); ?>"></a>
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="<?php echo e(url('hotel-details')); ?>">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/3.jpg');">
														<a href="<?php echo e(url('hotel-details')); ?>"></a>
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="<?php echo e(url('hotel-details')); ?>">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/3.jpg');">
														<a href="<?php echo e(url('hotel-details')); ?>"></a>
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="<?php echo e(url('hotel-details')); ?>">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/3.jpg');">
														<a href="<?php echo e(url('hotel-details')); ?>"></a>
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="<?php echo e(url('hotel-details')); ?>">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/3.jpg');">
														<a href="<?php echo e(url('hotel-details')); ?>"></a>
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="<?php echo e(url('hotel-details')); ?>">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/3.jpg');">
														<a href="<?php echo e(url('hotel-details')); ?>"></a>
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="<?php echo e(url('hotel-details')); ?>">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="card">
													<div class="card-image" style="background-image: url('public/assets/img/user/3.jpg');">
														<a href="<?php echo e(url('hotel-details')); ?>"></a>
														<div class="card-image-rating">
															<i class="md-icon">hotel</i>
															<i class="md-icon">restaurant_menu</i>
															<i class="md-icon">home</i>
														</div>
													</div>
													<div class="card-content">
														<h2><a href="<?php echo e(url('hotel-details')); ?>">Hotel Name Here</a></h2>
													</div>
												</div>
											</div>
										</div>
										<nav class="pagination-wrapper">
											<ul class="pagination">
												<li class="page-item disabled">
													<a class="page-link" href="<?php echo e(url('hotel-details')); ?>" aria-label="Previous">
														<span aria-hidden="true">&laquo;</span>
														<span class="sr-only">Previous</span>
													</a>
												</li>
												<li class="page-item">
													<a class="page-link" href="<?php echo e(url('hotel-details')); ?>">1 <span class="sr-only">(current)</span></a>
												</li>
												<li class="page-item active">
													<a class="page-link" href="<?php echo e(url('hotel-details')); ?>">2</a>
												</li>
												<li class="page-item">
													<a class="page-link" href="<?php echo e(url('hotel-details')); ?>">3</a>
												</li>
												<li class="page-item">
													<a class="page-link" href="<?php echo e(url('hotel-details')); ?>" aria-label="Next">
														<span aria-hidden="true">&raquo;</span>
														<span class="sr-only">Next</span>
													</a>
												</li>
											</ul>
										</nav>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-lg-3">
								<div class="sidebar">
									<div class="page-header page-header-small">
										<h3>Filter Listing</h3>
									</div>
									<div class="filter filter-white checkbox-light">
										<form method="get" action="?">
											<h2>Attributes</h2>
											<div class="checkbox">
												<label><input type="checkbox"> Recent</label>
											</div>
											<div class="checkbox">
												<label><input type="checkbox"> Featured</label>
											</div>
											<div class="form-group">
												<label>Location</label>
												<select class="form-control">
													<option>Location 1</option>
													<option>Location 2</option>
												</select>
											</div>
											<div class="form-group">
												<label>Keyword</label>
												<select class="form-control">
													<option>Keyword 1</option>
													<option>Keyword 2</option>
												</select>
											</div>		
										</form>
									</div>
									<div class="widget">
										<h2 class="widgettitle">Recent Listings</h2>
										<div class="cards-small-wrapper">
											<div class="card-small">
												<div class="card-small-image">
													<a href="<?php echo e(url('hotel-details')); ?>" style="background-image: url('public/assets/img/user/2.jpg');"></a>
												</div>
												<div class="card-small-content">
													<h3><a href="<?php echo e(url('hotel-details')); ?>">Cozzy Coffee Shop</a></h3>
													<h4><a href="<?php echo e(url('hotel-details')); ?>">Drink &amp; Food</a></h4>
												</div>
											</div>
											<div class="card-small">
												<div class="card-small-image">
													<a href="<?php echo e(url('hotel-details')); ?>" style="background-image: url('public/assets/img/user/3.jpg');"></a>
												</div>
												<div class="card-small-content">
													<h3><a href="<?php echo e(url('hotel-details')); ?>">Cozzy Coffee Shop</a></h3>
													<h4><a href="<?php echo e(url('hotel-details')); ?>">Drink &amp; Food</a></h4>
												</div>
											</div>
											<div class="card-small">
												<div class="card-small-image">
													<a href="<?php echo e(url('hotel-details')); ?>" style="background-image: url('public/assets/img/user/4.jpg');"></a>
												</div>
												<div class="card-small-content">
													<h3><a href="<?php echo e(url('hotel-details')); ?>">Cozzy Coffee Shop</a></h3>
													<h4><a href="<?php echo e(url('hotel-details')); ?>">Drink &amp; Food</a></h4>
												</div>
											</div>
											<div class="card-small">
												<div class="card-small-image">
													<a href="<?php echo e(url('hotel-details')); ?>" style="background-image: url('public/assets/img/user/5.jpg');"></a>
												</div>
												<div class="card-small-content">
													<h3><a href="<?php echo e(url('hotel-details')); ?>">Cozzy Coffee Shop</a></h3>
													<h4><a href="<?php echo e(url('hotel-details')); ?>">Drink &amp; Food</a></h4>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php echo $__env->make('includes/footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
<?php echo $__env->make('includes/btm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>