@include('admin.includes.header')
@php($add = 1)
@if(isset($data)!=0)
	@php($add = 0)
@endif
<body>
	<section class="body">
		<div class="inner-wrapper">
			@include('admin.includes.nav')
			<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Hotel</h2>
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.php"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Listing</span></li>
							<li><span>Business</span></li>
						</ol>
						<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>
				<form method="post" action="{{url('admin/hotel/saveupdate')}}" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="ID" value="{{ !$add ? $data['business'][0]->ID : '' }}" >
					<section class="panel">
						<div class="row">
							<div class="col-md-4 col-lg-3">
								<section class="panel">
									<div class="panel-body">
										<input type="hidden" name="business_pic" value="{{(!$add) ? $data['business'][0]->business_profile_pic : ''}}">
										<img src="{{!$add ? url('uploads/business/'.$data['business'][0]->business_profile_pic) : ''}}" class="proImg" alt="Business Logo">
										<div class="overlay">
											<label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
											<input type="file"  name="business_profile_pic" onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >

										</div>
									</div>
								</section>
							</div>
							<div class="col-md-8 col-lg-9">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
									</div>
									<h2 class="panel-title">Basic Information</h2>
								</header>
								<div class="panel-body">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Business Name</label>
												<input type="text" name="business_name" class="form-control" value="{{ !$add ? $data['business'][0]->business_name : '' }}">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Email</label>
												<input type="text" name="business_email" value="{{ !$add ? $data['business'][0]->business_email : '' }}" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Phone</label>
												<input type="email" name="business_phone" value="{{ !$add ? $data['business'][0]->business_phone : '' }}" class="form-control">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">URL</label>
												<input type="url" name="business_website_url" value="{{ !$add ? $data['business'][0]->business_website_url : '' }}" class="form-control">
											</div>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</section>
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Other Information</h2>
						</header>
						<div class="panel-body">
							<div class="row">										
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Map URL</label>
										<input type="text" name="map_url" value="{{ !$add ? $data['business'][0]->map_url : '' }}"  class="form-control">
									</div>
								</div>
							</div>	
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label">Latitude</label>
										<input name="latitude" value="{{ !$add ? $data['business'][0]->latitude : '' }}" type="text" class="form-control">
									</div>
								</div>								
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label">Longitude</label>
										<input name="longitude" value="{{ !$add ? $data['business'][0]->longitude : '' }}" type="text" class="form-control">
									</div>
								</div>
							</div>						
							<div class="row">										
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Facebook Page ID</label>
										<input type="text" name="facebook_page_id" value="{{ !$add ? $data['business'][0]->facebook_page_id : '' }}" class="form-control">
									</div>
								</div>
							</div>
							<div class="row">										
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Category</label>
										<select name="business_category" class="form-control">
											<option value="Hotel">Hotel</option>
											<option value="Restaurant">Restaurant</option>
											<option value="Lodge">Lodge</option>
											<option value="Homestay">Homestay</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">										
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Contact Address</label>
										<textarea name="contact_address" value="{{ !$add ? $data['business'][0]->contact_address : '' }}" class="form-control">{{ !$add ? $data['business'][0]->contact_address : '' }}</textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">About</label>
										<textarea name="about_business"  value="{{ !$add ? $data['business'][0]->about_business : '' }}" class="form-control">{{ !$add ? $data['business'][0]->about_business : '' }}</textarea>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Manage Amenities</h2>
						</header>
						<div class="panel-body">
							<div class="row">
								@foreach($allAmenities as $amenity)
								<div class="col-sm-3">
									<div class="checkbox">
										<label><input type="checkbox" @if(!$add ? $amenity->amenities == $amenity->amenities :'') checked @endif name="selected_amenities[]" value="{{ $amenity->amenities}}"> {{$amenity->amenities}}</label>
									</div>
								</div>
								@endforeach


							</div>
							<div class="row">
								<div class="col-sm-12">
									<a href="#">Edit Amenities</a>
								</div>
							</div>
						</div>
					</section>
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Manage Hours</h2>
						</header>
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-12">
									<div class="radio">
										<label>
											<input id="hourOne" type="radio" name="openingHours" value="option1" @if(!$add ? isset($business_hours[0]->always_open)== null : 'checked')checked @endif >
											Open on selected hour
										</label>
									</div>
									<div class="radio">
										<label>
											<input id="hourTwo" @if(!$add ? isset($business_hours[0]->always_open)=='1' : '')checked @endif  type="radio" name="openingHours" value="option2">
											Always open
										</label>
									</div>
								</div>
								<div class="col-sm-offset-2 col-sm-8 hoursDetail" id="hoursDetail">
									<table class="table">
										<tbody>
											<tr>
												<td><strong>Sunday</strong></td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" name="sunday_opening_time" value="{{ !$add ? isset($business_hours[0]->sunday_opening_time) : '' }}">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->sunday_closing_time) : '' }}" name="sunday_closing_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
											</tr>
											<tr>
												<td><strong>Monday</strong></td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->monday_opening_time) : '' }}" name="monday_opening_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->monday_closing_time) : '' }}" name="monday_closing_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
											</tr>
											<tr>
												<td><strong>Tuesday</strong></td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->tuesday_opening_time) : '' }}" name="tuesday_opening_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->tuesday_closing_time) : '' }}" name="tuesday_closing_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
											</tr>
											<tr>
												<td><strong>Wednesday</strong></td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->wednesday_opening_time) : '' }}" name="wednesday_opening_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->wednesday_closing_time) : '' }}" name="wednesday_closing_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
											</tr>
											<tr>
												<td><strong>Thursday</strong></td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->thursday_opening_time) : '' }}" name="thursday_opening_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->thursday_closing_time) : '' }}" name="thursday_closing_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
											</tr>
											<tr>
												<td><strong>Friday</strong></td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->friday_opening_time) : '' }}" name="friday_opening_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->friday_closing_time) : '' }}" name="friday_closing_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
											</tr>
											<tr>
												<td><strong>Saturday</strong></td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->saturday_opening_time) : '' }}" name="saturday_opening_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group bootstrap-timepicker timepicker">
														<input class="timepicker1 form-control input-sm" type="text" value="{{ !$add ? isset($business_hours[0]->saturday_closing_time) : '' }}" name="saturday_closing_time">
														<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</section>
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Gallery Image</h2>
						</header>
						<div class="panel-body">
							@if(!$add)
								@if($businessImage!=='')
								@foreach($businessImage as $business_image)
							<div class="row">
								<div class="col-sm-3">
									<img src="{{!$add ? url('uploads/business/'.$business_image->business_image):''}}" class="proImg" alt="Gallery Image">
									<div class="overlay">
										<label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
										<input type="file"  name="business_image[]" value="{{(!$add) ? $business_image->business_image : '' }}"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
									</div>
								</div>
								<div class="col-sm-9">
									<div class="form-group">
										<label class="control-label">Image Title</label>
										<input type="hidden" name="businessImageID" value="{{(!$add) ? $business_image->ID : '' }}">
										<input type="text" name="image_title[]" class="form-control" value="{{(!$add) ? $business_image->image_title : '' }}">
									</div>
									<div class="form-group">
										<label class="control-label">Image Description</label>
										<input type="text" name="image_description[]" value="{{(!$add) ? $business_image->image_description : '' }}" class="form-control">
									</div><br>
									<button type="button" class="removeImage btn btn-danger"><i class="fa fa-trash"></i> Delete Image</button><hr>
								</div>
							</div>
								@endforeach
									@endif
							@endif
							<div class="row">
								<div class="col-sm-12">
									<button type="button" class="addNewImage btn btn-primary"><i class="fa fa-plus"></i> Add New Image</button>
								</div>
							</div>
						</div>
					</section>
					<div class="addNew">
						<button class="" type="submit"><i class="fa fa-save"></i> Save</button>
					</div>
				</form>
			</section>
		</div>
	</section>
	
	<script src="{{url('admin-assets/vendor/jquery/jquery.js')}}"></script>
	<script src="{{url('admin-assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
	<script src="{{url('admin-assets/vendor/jquery-cookie/jquery-cookie.js')}}"></script>
	<script src="{{url('admin-assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
	<script src="{{url('admin-assets/vendor/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
	<script src="{{url('admin-assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
	<script src="{{url('admin-assets/vendor/jquery-placeholder/jquery-placeholder.js')}}"></script>
	<script src="{{url('admin-assets/vendor/select2/js/select2.js')}}"></script>
	<script src="{{url('admin-assets/javascripts/theme.js')}}"></script>
	<script src="{{url('admin-assets/javascripts/theme.custom.js')}}"></script>
	<script src="{{url('admin-assets/javascripts/theme.init.js')}}"></script>
<script>
    $('.addNewImage').click(function() {
        $(this).before('<div class="row"><div class="col-sm-3"><img src="http://lorempixel.com/200/200/nature/" class="proImg" alt="Gallery Image"><div class="overlay"><label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label><input type="file" name="business_image[]"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" ></div></div><div class="col-sm-9"><div class="form-group"><label class="control-label">Image Title</label><input type="text" name="image_title[]" class="form-control"></div><div class="form-group"><label class="control-label">Image Description</label><input type="text" name="image_description[]" class="form-control"></div><br><button type="button" class="removeImage btn btn-danger"><i class="fa fa-trash"></i> Delete Image</button><hr></div></div>');
        $('#whatever').append(structure);
    });
</script>
</body>

</html>