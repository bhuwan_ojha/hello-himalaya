<aside class="sidebar-left" id="sidebar-left">
    <div class="sidebar-header">
        <div class="sidebar-title">
            MENU
        </div>
        <div class="sidebar-toggle hidden-xs" data-fire-event="sidebar-left-toggle" data-target="html" data-toggle-class="sidebar-left-collapsed">
            <i aria-label="Toggle sidebar" class="fa fa-bars"></i>
        </div>
    </div>
    <div class="nano">
        <div class="nano-content">
            <nav class="nav-main" id="menu" role="navigation">
                <ul class="nav nav-main">
                    <li class="nav-active">
                        <a href="#"><i aria-hidden="true" class="fa fa-home"></i> <span>Dashboard</span></a>
                    </li>
                    <li>
                        <a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-envelope"></i> <span>Messages</span></a>
                    </li>
                    <li>
                        <a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-users"></i> <span>Contacts</span></a>
                    </li>
                    <li class="nav-parent">
                        <a><i aria-hidden="true" class="fa fa-columns"></i> <span>Manage Listing</span></a>
                        <ul class="nav nav-children">
                            <li><a href="<?php echo e(url('admin/guide-list')); ?>">Climbers</a></li>
                            <li><a href="<?php echo e(url('admin/hotel-list')); ?>">Business</a></li>
                            <li><a href="<?php echo e(url('admin/manage-category')); ?>">Categories</a></li>
                            <li><a href="<?php echo e(url('admin/amenities')); ?>">Amenities</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-users"></i> <span>Manage pages</span></a>
                    </li>
                    <li>
                        <a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-users"></i> <span>calendar</span></a>
                    </li>
                    <li>
                        <a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-users"></i> <span>Manage Comments</span></a>
                    </li>

                </ul>
            </nav>
        </div>
        <script>
            // Maintain Scroll Position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');
                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>
    </div>
</aside>