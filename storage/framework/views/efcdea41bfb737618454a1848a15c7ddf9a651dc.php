					<div class="header-top">
						<div class="container">
							<a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(url('public/assets/img/logo.svg')); ?>" alt="" class="header-top-logo"></a>
							<div class="nav-primary-wrapper collapse navbar-toggleable-sm">
								<ul class="nav nav-pills nav-right">
									<li class="nav-item"><a href="<?php echo e(url('/')); ?>" class="nav-link active">Home</a></li>
									<li class="nav-item"><a href="<?php echo url('travel-guide');?>" class="nav-link ">Travel Guide</a></li>
									<li class="nav-item has-submenu">
										<a href="#" class="nav-link ">Listing</a>
										<ul class="submenu">
											<li><a href="<?php echo e(url('featured-list')); ?>">Featured</a></li>
											<li><a href="<?php echo e(url('recent-list')); ?>">Recent</a></li>
											<li><a href="<?php echo e(url('popular-list')); ?>">Popular</a></li>
										</ul>
									</li>
								</ul>
							</div>
							<button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target=".nav-primary-wrapper">
								<i class="md-icon">menu</i>
							</button>						
						</div>
					</div>