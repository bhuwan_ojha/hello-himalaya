@include('admin.includes.header')
<body>
<section class="body">
	<div class="inner-wrapper">
		@include('admin.includes.nav')
			<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Guides</h2>					
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.php"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Listing</span></li>
							<li><span>Business</span></li>
						</ol>
						<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>
				<form method="post" action="{{url('admin/add-category')}}" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Manage Guides Categories</h2>
						</header>
						<div class="panel-body">
							<div class="row">										
								<select multiple data-role="tagsinput" name="guide_category[]">
									@foreach($data as $value)
									<option value="{{$value->guide_category}}">{{$value->guide_category}}</option>
									@endforeach
								</select>
							</div>	
						</div>
					</section>
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Manage Business Categories</h2>
						</header>
						<div class="panel-body">
							<div class="row">										
								<select multiple data-role="tagsinput" name="business_category[]">
									@foreach($data as $value)
										<option value="{{$value->business_category}}">{{$value->business_category}}</option>
									@endforeach
								</select>
							</div>	
						</div>
					</section>
					<div class="addNew">
						<button class="" type="submit"><i class="fa fa-save"></i> Save</button>
					</div>
				</form>
			</section>
		</div>
	</section>
	
	@include('admin.includes.footer')
</body>
</html>