		<div class="footer-wrapper">
			<div class="footer">
				<div class="footer-widgets">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-2 col-md-offset-1">
								<div class="widget right-border">
									<h2 class="widgettitle">Links</h2>
									<ul class="nav nav-stacked">
										<li class="nav-item"><a href="#" class="nav-link">Company</a></li>
										<li class="nav-item"><a href="#" class="nav-link">Our Team</a></li>
										<li class="nav-item"><a href="#" class="nav-link">Partners</a></li>
										<li class="nav-item"><a href="#" class="nav-link">Affiliate</a></li>
									</ul>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-2 col-md-offset-1">
								<div class="widget right-border">
									<h2 class="widgettitle">&nbsp;</h2>
									<ul class="nav nav-stacked">
										<li class="nav-item"><a href="#" class="nav-link">Career</a></li>
										<li class="nav-item"><a href="#" class="nav-link">Press</a></li>
										<li class="nav-item"><a href="#" class="nav-link">CSR</a></li>
										<li class="nav-item"><a href="#" class="nav-link">Advertise</a></li>
									</ul>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-2 col-md-offset-1">
								<div class="widget right-border">
									<h2 class="widgettitle">&nbsp;</h2>
									<ul class="nav nav-stacked">
										<li class="nav-item"><a href="#" class="nav-link">Support</a></li>
										<li class="nav-item"><a href="#" class="nav-link">FAQs</a></li>
										<li class="nav-item"><a href="#" class="nav-link">Pricing</a></li>
										<li class="nav-item"><a href="#" class="nav-link">Contact Us</a></li>
									</ul>
								</div>
							</div>			
							<div class="col-xs-12 col-sm-12 col-md-2 col-md-offset-1">
								<div class="widget">
									<h2 class="widgettitle">&nbsp;</h2>
									<ul class="nav nav-stacked">
										<li class="nav-item"><a href="#" class="nav-link">Site Map</a></li>
										<li class="nav-item"><a href="#" class="nav-link">Legal Information</a></li>
										<li class="nav-item"><a href="#" class="nav-link">Privacy Policy</a></li>
										<li class="nav-item"><a href="#" class="nav-link">Terms & Conditions</a></li>
									</ul>
								</div>
							</div>		
						</div>
					</div>
				</div>
				<div class="footer-bottom">
					<div class="container">
						<div class="footer-bottom-left">
							Powere by: <a href="#">Karma Tech Solutions</a>
						</div>
						<div class="footer-bottom-right">
							Copyright &copy; 2017 - All Rights Reserved
						</div>		
					</div>
				</div>
			</div>
		</div>