<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Hotel;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HotelController extends Controller
{
    public function __construct()
    {
        $this->hotel = new Hotel();
    }

    public function ListHotel()
    {
        $data = $this->hotel->FindAll();
        return view('admin/hotel-list',['data'=>$data]);
    }

    public function addHotel()
    {
        $amenities = $this->hotel->get_distinct_amenities();
        return view('admin/hotel',['allAmenities'=>$amenities]);
    }

    public function Amenities()
    {
        $data = $this->hotel->get_bussiness_categories();
        return view('admin/amenities',['data'=>$data]);
    }

    public function AddAmenities(Request $request)
    {
        $data = $request->input();
        $this->hotel->addAmenities($data);
        return redirect('admin/amenities');
    }

    public function Managecategory()
    {
        $data = $this->hotel->get_categories();
        return view('admin/categories',['data'=>$data]);
    }

    public function AddCategory(Request $request)
    {
        $business_category = array(
          'business_category' => $request->input('business_category')
        );

        $guide_category = array(
            'guide_category'=>$request->input('guide_category')
        );
        if ($request->input('ID') != "") {

            $id = $request->input('ID');
            $this->hotel->SaveUpdate($business_category, $guide_category, $id);
        } else {
            $this->hotel->SaveUpdate($business_category, $guide_category);
        }

        return redirect('admin/manage-category');
    }

    public function SaveUpdate(Request $request)
    {
        if($_POST['business_pic']!==null){
            $filename = $_POST['business_pic'];
        }else{
            $filename = '';

        }
        if ($request->file('business_profile_pic')) {
            $file = $request->file('business_profile_pic');
            $destinationPath = public_path() . '/uploads/business/';
            $filename = time() . '_' . $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $fileName = $file->move($destinationPath, $filename);
        }
        $hotel_data = array(
            'ID'=>$request->input('ID'),
            'business_name'=>$request->input('business_name'),
            'business_email'=>$request->input('business_email'),
            'business_phone'=>$request->input('business_phone'),
            'business_website_url'=>$request->input('business_website_url'),
            'map_url'=>$request->input('map_url'),
            'latitude'=>$request->input('latitude'),
            'longitude'=>$request->input('longitude'),
            'facebook_page_id'=>$request->input('facebook_page_id'),
            'business_category'=>$request->input('business_category'),
            'contact_address'=>$request->input('contact_address'),
            'about_business'=>$request->input('about_business'),
            'business_profile_pic' => $filename
        );

        if($request->input('openingHours')=='option1') {
            $hours = array(
                'sunday_opening_time' => $request->input('sunday_opening_time'),
                'sunday_closing_time' => $request->input('sunday_closing_time'),
                'monday_closing_time' => $request->input('monday_closing_time'),
                'monday_opening_time' => $request->input('monday_opening_time'),
                'tuesday_closing_time' => $request->input('tuesday_closing_time'),
                'tuesday_opening_time' => $request->input('tuesday_opening_time'),
                'wednesday_closing_time' => $request->input('wednesday_closing_time'),
                'wednesday_opening_time' => $request->input('wednesday_opening_time'),
                'thursday_closing_time' => $request->input('thursday_closing_time'),
                'thursday_opening_time' => $request->input('thursday_opening_time'),
                'friday_closing_time' => $request->input('friday_closing_time'),
                'friday_opening_time' => $request->input('friday_opening_time'),
                'saturday_closing_time' => $request->input('saturday_closing_time'),
                'saturday_opening_time' => $request->input('saturday_opening_time'),
            );
        }else{
         $hours = array('always_open'=>'1');
        }


            $amenities = $request->input('selected_amenities');

        $business_gallery = array();
        if($request->file('business_image')!==null) {
                foreach ($request->file('business_image') as $data_key => $data_value) {
                    if ($request->file('business_image')[$data_key] !== null) {
                        $file = $request->file('business_image')[$data_key];

                        $destinationPath = public_path() . '/uploads/business';
                        $filename = time() . '_' . $file->getClientOriginalName();
                        $filename = str_replace(' ', '_', $filename);
                        $fileName = $file->move($destinationPath, $filename);
                        $business_gallery[] = array(
                            'business_image' => $filename,
                            'image_title' => $request->input('image_title')[$data_key],
                            'image_description' => $request->input('image_description')[$data_key],
                            'new_name' => $filename,
                            'ID' => $request->input('businessImageID')
                        );


                    }
                }
            }


        if($request->input('ID')!==0){
            $this->hotel->SaveUpdateBussiness($hotel_data,$hours,$business_gallery,$amenities,$_POST['ID']);

        }else{
            $this->hotel->SaveUpdateBussiness($hotel_data,$hours,$business_gallery,$amenities);

        }

        //return redirect('/admin/hotel-list');


    }

    public function getById($id)
    {
        $data['data'] = $this->hotel->getByID($id);
        $data['allAmenities'] = $this->hotel->get_distinct_amenities();
        $data['amenities'] = $this->hotel->get_amneties_byID($id);
        $data['businessImage'] = $this->hotel->get_businessImage($id);
        $data['business_hours'] = $this->hotel->get_business_hours($id);
        return view('admin/hotel',$data);
    }

    public function DeleteHotel(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_business')->where('ID', $id)->delete();
        $result = DB::table('selected_amenities')->where('business_id', $id)->delete();
        $result = DB::table('business_gallery')->where('business_id', $id)->delete();
        if ($result){
            return  redirect('admin/hotel-list');
        }
    }

    public function UpdatePublishStatus(Request $request)
    {
        $status = $request->input('status');
        $id = $request->input('id');
        if($status!=''){
            DB::table('tbl_business')->where('ID', $id)->update(['status' => $status]);
        }
    }


}

