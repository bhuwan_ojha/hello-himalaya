
	@php($pgName = 'Business Detail | Hello Himalayn Homes')
	@include('includes.top')

<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=784260374946984";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
	<div class="page-wrapper">
		<div class="header-wrapper">
			<div class="header">
				<div class="header-inner">
					@include('includes/nav')
					@include('includes/nav1')
				</div>
			</div>
		</div>
		<div class="main-wrapper">
			<div class="main">
				<div class="main-inner">
					<div class="content">
						<div class="content-title">
							<div class="container">
								<h1>Hotel Name Here</h1>
								<ul class="breadcrumb">
									<li><a href="index.blade.php">Home</a> <i class="md-icon">keyboard_arrow_right</i></li>
									<li><a href="list.blade.php">Listing</a> <i class="md-icon">keyboard_arrow_right</i></li>
									<li class="active">Detail</li>
								</ul>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-lg-9">
									<div class="push-top-bottom">
										<div class="listing-detail">
											<div class="gallery">
												<div class="gallery-item" style="background-image: url('public/assets/img/gallery/1.jpg');">
													<div class="gallery-item-description">
														Quisque ornare efficitur risus eu bibendum
													</div>
												</div>
												<div class="gallery-item" style="background-image: url('public/assets/img/gallery/3.jpg');">
													<div class="gallery-item-description">
														Quisque ornare efficitur risus eu bibendum
													</div>
												</div>        
											</div>
											<h2>Property Description</h2>
											<p>Duis suscipit turpis suscipit lectus ultricies, placerat fermentum orci fringilla. Mauris semper magna cursus diam suscipit consectetur.</p>
											<p>Integer condimentum sodales metus, quis pellentesque nisi tempor at. Quisque ornare efficitur risus eu bibendum. Donec feugiat consequat magna in interdum. Donec malesuada orci et mi fermentum, ac tincidunt arcu tincidunt. Ut fringilla tellus sem, quis rhoncus dolor condimentum id. Vivamus ut commodo felis. Fusce in lacus egestas, mattis ex et, molestie est.</p>
											<h2>Amenities</h2>
											<ul class="amenities">
												<li class="yes">Conditioning</li>
												<li class="no">Balcony</li>
												<li class="no">Bedding</li>
												<li class="no">Cable TV</li>
												<li class="no">Cleaning</li>
												<li class="no">Cofee pot</li>
												<li class="no">Computer</li>
												<li class="yes">Cot</li>
												<li class="no">Dishwasher</li>
												<li class="yes">DVD</li>
												<li class="no">Fan</li>
												<li class="yes">Fridge</li>
												<li class="no">Grill</li>
												<li class="yes">Hairdryer</li>
												<li class="no">Heating</li>
												<li class="yes">Hi-fi</li>
												<li class="no">Internet</li>
												<li class="yes">Iron</li>
												<li class="no">Juicer</li>
												<li class="yes">Lift</li>
												<li class="yes">Microwave</li>
												<li class="yes">Oven</li>
												<li class="no">Parking</li>
												<li class="no">Parquet</li>
												<li class="yes">Pool</li>
												<li class="yes">Radio</li>
												<li class="no">Roof terrace</li>
												<li class="no">Smoking</li>
												<li class="yes">Terrace</li>
												<li class="no">Toaster</li>
												<li class="no">Towelwes</li>
												<li class="no">Use of pool</li>
											</ul>	
											<h2>Opening Hours</h2>
											<div class="opening-hours-wrapper">
												<table class="opening-hours">
													<thead>
														<tr>
															<th>MONDAY</th>
															<th>TUESDAY</th>
															<th>WEDNESDAY</th>
															<th>THURSDAY</th>
															<th>FRIDAY</th>
															<th>SATURDAY</th>
															<th>SUNDAY</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td class="open">
																<span class="from">8:00 am</span> <span class="separator">-</span> <span class="to">8:00 pm</span>                        
															</td>
															<td class="other-day">
																<span class="from">9:00 am</span> <span class="separator">-</span> <span class="to">9:00 pm</span>                        
															</td>
															<td class="other-day">
																<span class="from">8:00 am</span> <span class="separator">-</span> <span class="to">3:00 pm</span>                        
															</td>
															<td class="other-day">
																<span class="from">7:00 am</span> <span class="separator">-</span> <span class="to">9:00 pm</span>                        
															</td>
															<td class="other-day">
																<span class="from">10:00 am</span> <span class="separator">-</span> <span class="to">5:00 pm</span>                        
															</td>
															<td class="other-day">
																<span class="from">8:00 am</span> <span class="separator">-</span> <span class="to">11:00 am</span>                        
															</td>
															<td class="other-day">closed</td>
														</tr>
													</tbody>
												</table>
											</div>
											<h2>Location in Map</h2>
											<div id="listing-position"></div>		
											<h2>3 comments in this topic</h2>
											<ul class="comments">
												@foreach($comment as $value)

												<li>
													<div class="comment">			
														<div class="comment-author">
															<a href="#" style="background-image: url('{{url($value->image)}}"></a>
														</div>
														<div class="comment-content">			
															<div class="comment-meta">
																<div class="comment-meta-author">
																	Posted by <a href="#">{{$value->username}}</a>
																</div>
																<div class="comment-meta-reply">
																	<a href="#">Reply</a>
																</div>
																<div class="comment-meta-date">
																	<span>{{$value->created_date}}</span>
																</div>
															</div>
															<div class="comment-body">
																{{$value->message}}
															</div>
														</div>
													</div>		
													{{--<ul>
														<li>
															<div class="comment">			
																<div class="comment-author">
																	<a href="#" style="background-image: url('public/assets/img/tmp/profile-2.jpg');"></a>
																</div>
																<div class="comment-content">			
																	<div class="comment-meta">
																		<div class="comment-meta-author">
																			Posted by <a href="#">admin</a>
																		</div>
																		<div class="comment-meta-reply">
																			<a href="#">Reply</a>
																		</div>
																		<div class="comment-meta-date">
																			<span>8:54 PM 11/23/2016</span>
																		</div>
																	</div>
																	<div class="comment-body">
																		<div class="comment-rating">
																			<i class="fa fa-star"></i>
																			<i class="fa fa-star"></i>
																			<i class="fa fa-star"></i>
																			<i class="fa fa-star-half-o"></i>
																			<i class="fa fa-star-o"></i>
																		</div>
																		Donec malesuada orci et mi fermentum, ac tincidunt arcu tincidunt. Ut fringilla tellus sem, quis rhoncus dolor condimentum id. Vivamus ut commodo felis. Fusce in lacus egestas, mattis ex et, molestie est.
																	</div>
																</div>
															</div>									
														</li>
													</ul>--}}
													@endforeach
												</li>
											</ul>
											<h2>Write Feedback</h2>
											<span><a href="{{url('login/facebook')}}" class="btn btn-sm btn-primary">Login  </a> With Facebook To Post</span>
											<div class="comment-create clearfix" id="message-box">
												<form method="post" action="{{url('/comment-submit')}}">
													<input type="hidden" name="_token" value="{{ csrf_token() }}">
													<input type="hidden" name="user-id" id="user-id" value="{{($session) ?  $session:''}}">
													<div class="form-group">
														<label>Message</label>
														<textarea name="message"  class="form-control" rows="5"></textarea>
													</div>
													<div class="row">
													</div>
													<div class="form-group-btn">
														<button type="submit" class="btn btn-primary btn-large pull-right">Post Comment</button>
													</div>
												</form>
											</div>											
										</div>
									</div>
								</div>
								<div class="col-md-4 col-lg-3">
									<div class="sidebar">
										<div class="widget profile">
											<img src="public/assets/img/profile/1.png" alt=" Business Logo">
										</div>
										<div class="widget">
											<h2 class="widgettitle">Contact Information</h2>
											<ul class="contact">
												<li><i class="md-icon">email</i> <a href="mailto:hi@example.com">hi@example.com</a></li>
												<li><i class="md-icon">link</i> <a href="#">example.com</a></li>
												<li><i class="md-icon">phone</i> +01-23-456-789</li>
												<li><i class="md-icon">location_on</i> Some Street<br>Lukla, Solukhumbu</li>
											</ul>
										</div>
										<div class="widget social-fb">
											<div class="fb-page" data-href="https://www.facebook.com/HelloHimalayanHomes" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/HelloHimalayanHomes" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/HelloHimalayanHomes">Hello Himalayan Homes</a></blockquote></div>
										</div>										
										<div class="widget widget-background-white">
											<h3 class="widgettitle">Inquire Form</h3>
											<form method="post" action="?">
												<div class="form-group">
													<label>Full Name</label>
													<input type="text" class="form-control">
												</div>
												<div class="form-group">
													<label>E-mail</label>
													<input type="email" class="form-control">
												</div>
												<div class="form-group">
													<label>Subject</label>
													<input type="text" class="form-control">
												</div>             
												<div class="form-group">
													<label>Message</label>
													<textarea class="form-control" rows="4"></textarea>
												</div>                              
												<div class="form-group-btn">
													<button type="submit" class="btn btn-primary btn-block btn-large">Send Message</button>
												</div>
											</form>
										</div>
										<div class="widget">
										<h2 class="widgettitle">Recent Listings</h2>
										<div class="cards-small-wrapper">
											<div class="card-small">
												<div class="card-small-image">
													<a href="#" style="background-image: url('public/assets/img/user/2.jpg');"></a>
												</div>
												<div class="card-small-content">
													<h3><a href="#">Cozzy Coffee Shop</a></h3>		
													<h4><a href="#">Drink &amp; Food</a></h4>
												</div>
											</div>
											<div class="card-small">
												<div class="card-small-image">
													<a href="#" style="background-image: url('public/assets/img/user/3.jpg');"></a>
												</div>
												<div class="card-small-content">
													<h3><a href="#">Cozzy Coffee Shop</a></h3>		
													<h4><a href="#">Drink &amp; Food</a></h4>
												</div>
											</div>
											<div class="card-small">
												<div class="card-small-image">
													<a href="#" style="background-image: url('public/assets/img/user/4.jpg');"></a>
												</div>
												<div class="card-small-content">
													<h3><a href="#">Cozzy Coffee Shop</a></h3>		
													<h4><a href="#">Drink &amp; Food</a></h4>
												</div>
											</div>
											<div class="card-small">
												<div class="card-small-image">
													<a href="#" style="background-image: url('public/assets/img/user/5.jpg');"></a>
												</div>
												<div class="card-small-content">
													<h3><a href="#">Cozzy Coffee Shop</a></h3>		
													<h4><a href="#">Drink &amp; Food</a></h4>
												</div>
											</div>
										</div>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<form method="post" action="?">
							<div class="form-group">
								<label>Post Comment</label>
								<textarea class="form-control" rows="5"></textarea>
							</div>
							<button type="button" class="btn btn-primary">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		@include('includes/footer')
	</div>
	@include('includes/btm')
	<script>
		$(document).ready(function () {
			if($('#user-id').val()!==""){
			    $('#message-box').show();
			}else{
                $('#message-box').hide();
			}
        });
	</script>