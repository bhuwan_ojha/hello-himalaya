<!DOCTYPE html>
<html class="fixed">
<head>
    <meta charset="UTF-8">
    <title>Dashboard | Hello Himalayan Homes</title>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta content="Karma Tech Solutions Admin panel" name="keywords">
    <meta content="CMS for Hello Himalayan Homes by Karma Tech Solutions" name="description">
    <meta content="Karma Tech Solutions" name="author">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo e(url('admin-assets/vendor/bootstrap/css/bootstrap.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('admin-assets/vendor/font-awesome/css/font-awesome.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('admin-assets/vendor/magnific-popup/magnific-popup.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('admin-assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('admin-assets/vendor/select2/css/select2.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('admin-assets/vendor/select2-bootstrap-theme/select2-bootstrap.min.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('admin-assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('admin-assets/vendor/bootstrap-toggle/bootstrap-toggle.min.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('admin-assets/stylesheets/theme.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('admin-assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('admin-assets/stylesheets/theme-custom.css')); ?>">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo e(url('admin-assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')); ?>" />
    <script src="<?php echo e(url('admin-assets/vendor/ckeditor/ckeditor.js')); ?>"></script>
    <script src="<?php echo e(url('admin-assets/vendor/modernizr/modernizr.js')); ?>"></script>
    <script src="<?php echo e(url('admin-assets/vendor/style-switcher/style.switcher.localstorage.js')); ?>"></script>
</head>
<header class="header">
    <div class="logo-container">
        <a class="logo" href="index.php">
            <img alt="Porto Admin" height="35" src="<?php echo e(url('logo.svg')); ?>">
        </a>
        <div class="visible-xs toggle-sidebar-left" data-fire-event="sidebar-left-opened" data-target="html" data-toggle-class="sidebar-left-opened">
            <i aria-label="Toggle sidebar" class="fa fa-bars"></i>
        </div>
    </div>
    <div class="header-right">
        <ul class="notifications">
            <li>
                <a class="dropdown-toggle notification-icon" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope"></i> <span class="badge">4</span>
                </a>
                <div class="dropdown-menu notification-menu">
                    <div class="notification-title">
                        <span class="pull-right label label-default">230</span> Messages
                    </div>
                    <div class="content">
                        <ul>
                            <li>
                                <a class="clearfix" href="#">
                                    <figure class="image">
                                        <i class="fa fa-inbox bg-success"></i>
                                    </figure>
                                    <span class="title">Joseph Doe</span>
                                    <span class="message">Lorem ipsum dolor sit.</span>
                                </a>
                            </li>
                            <li>
                                <a class="clearfix" href="#">
                                    <figure class="image">
                                        <i class="fa fa-inbox bg-success"></i>
                                    </figure>
                                    <span class="title">Joseph Doe</span>
                                    <span class="message">Lorem ipsum dolor sit.</span>
                                </a>
                            </li>
                            <li>
                                <a class="clearfix" href="#">
                                    <figure class="image">
                                        <i class="fa fa-inbox bg-success"></i>
                                    </figure>
                                    <span class="title">Joseph Doe</span>
                                    <span class="message">Lorem ipsum dolor sit.</span>
                                </a>
                            </li>
                            <li>
                                <a class="clearfix" href="#">
                                    <figure class="image">
                                        <i class="fa fa-inbox bg-success"></i>
                                    </figure>
                                    <span class="title">Joseph Doe</span>
                                    <span class="message">Lorem ipsum dolor sit.</span>
                                </a>
                            </li>
                        </ul>
                        <hr>
                        <div class="text-right">
                            <a class="view-more" href="#">View All</a>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <a class="dropdown-toggle notification-icon" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell"></i> <span class="badge">3</span>
                </a>
                <div class="dropdown-menu notification-menu">
                    <div class="notification-title">
                        <span class="pull-right label label-default">3</span> Alerts
                    </div>
                    <div class="content">
                        <ul>
                            <li>
                                <a class="clearfix" href="#">
                                    <div class="image">
                                        <i class="fa fa-thumbs-down bg-danger"></i>
                                    </div><span class="title">Server is Down!</span> <span class="message">Just now</span></a>
                            </li>
                            <li>
                                <a class="clearfix" href="#">
                                    <div class="image">
                                        <i class="fa fa-lock bg-warning"></i>
                                    </div><span class="title">User Locked</span> <span class="message">15 minutes ago</span></a>
                            </li>
                            <li>
                                <a class="clearfix" href="#">
                                    <div class="image">
                                        <i class="fa fa-signal bg-success"></i>
                                    </div><span class="title">Connection Restaured</span> <span class="message">10/10/2016</span></a>
                            </li>
                        </ul>
                        <hr>
                        <div class="text-right">
                            <a class="view-more" href="#">View All</a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <span class="separator"></span>
        <div class="userbox" id="userbox">
            <a data-toggle="dropdown" href="#">
                <figure class="profile-picture">
                    <i class="img-circle fa fa-user"></i>
                </figure>
                <div class="profile-info" data-lock-email="johndoe@okler.com" data-lock-name="John Doe">
                    <span class="name">John Doe Junior</span> <span class="role">administrator</span>
                </div>
                <i class="fa custom-caret"></i>
            </a>
            <div class="dropdown-menu">
                <ul class="list-unstyled">
                    <li class="divider"></li>
                    <li>
                        <a href="#" role="menuitem" tabindex="-1"><i class="fa fa-user"></i> My Profile</a>
                    </li>
                    <li>
                        <a href="<?php echo e(url('admin/logout')); ?>" role="menuitem" tabindex="-1"><i class="fa fa-power-off"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>