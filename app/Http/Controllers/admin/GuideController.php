<?php

namespace App\Http\Controllers\admin;

use App\Guides;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Foundation\Http\FormRequest;



class GuideController extends Controller
{

    private $guide;
    public function __construct()
    {


        $this->guide = new Guides();

    }



    public function ListGuide()
    {
        $data = $this->guide->FindAll();
        return view('admin/guide-list',['data'=>$data]);
    }



    public function SaveUpdate(Request $request)
    {
        if($_POST['guide_pic']!==null){
            $filename = $_POST['guide_pic'];
        }else{
            $filename = '';

        }
        if ($request->file('guide_pic')) {
            $file = $request->file('guide_pic');
            $destinationPath = public_path() . '/uploads/';
            $filename = time() . '_' . $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $fileName = $file->move($destinationPath, $filename);
        }
        $data = array(
            'ID' => $request->input('ID'),
            'guide_name' => $request->input('guide_name'),
            'guide_about' => $request->input('guide_about'),
            'guide_description' => $request->input('guide_description'),
            'guide_category' => $request->input('guide_category'),
            'guide_email' => $request->input('guide_email'),
            'guide_website_url' => $request->input('guide_website_url'),
            'guide_phone' => $request->input('guide_phone'),
            'guide_location' => $request->input('guide_location'),
            'created_date' => getCurrentDate(),
            'guide_pic' => $filename
        );

        $timelineData['timeline_title'] =array();
        $timelineData = array(
                'timeline_title' => $request->input('timeline_title'),
                'timeline_description' => $request->input('timeline_description'),
                'date_time' => $request->input('date_time'),
                'ID' => $request->input('timelineID')
            );

        $gallery_image=array();


       // if ($request->file('guide_image')[0]!==null) {
            foreach ($request->file('guide_image') as $data_key => $data_value) {
                if ($request->file('guide_image')[$data_key]!==null) {
                    $file = $request->file('guide_image')[$data_key];


                    $destinationPath = public_path() . '/uploads/';
                    $filename = time() . '_' . $file->getClientOriginalName();
                    $filename = str_replace(' ', '_', $filename);
                    $fileName = $file->move($destinationPath, $filename);
                    $gallery_image[] = array(
                        'guide_image' =>  $filename,
                        'image_title' => $request->input('image_title')[$data_key],
                        'image_description' => $request->input('image_description')[$data_key],
                        'new_name' => $filename,
                        'ID' => $request->input('imageID')
                    );
                }
            }
        //}

            if ($data['ID'] != "") {
                $id = $data['ID'];
                $this->guide->SaveUpdate($data, $timelineData, $gallery_image, $id);
            } else {
                $this->guide->SaveUpdate($data, $timelineData,$gallery_image);
            }

        return redirect('/admin/guide-list');

    }

    public function DeleteGuide(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_guides')->where('ID', $id)->delete();
        $result = DB::table('guide_timeline')->where('guide_ID', $id)->delete();
        $result = DB::table('guide_gallery')->where('guide_ID', $id)->delete();
        if ($result){
          return  redirect('admin/guide');
        }
    }

    public function deleteGuideAchievement(Request $request)
    {
      $id = $request->input('id');
      if($id!==0){
          DB::table('guide_timeline')->where('ID', $id)->delete();
      }

    }
    public function deleteGalleryImage(Request $request)
    {
        $id = $request->input('id');
        if($id!==0){
            $image = DB::table('guide_gallery')->where('ID',$id)->get();
            $result = DB::table('guide_gallery')->where('ID', $id)->delete();
            if($result){
                unlink(public_path().'uploads/'.$image[0]->guide_image);
            }
        }

    }

    public function getById($id)
    {
        $data = $this->guide->getByID($id);
        return view('admin/detail',['data'=>$data]);
    }

    public function UpdatePublishStatus(Request $request)
    {
        $status = $request->input('status');
        $id = $request->input('id');
        if($status!=''){
            DB::table('tbl_guides')->where('ID', $id)->update(['status' => $status]);
        }
    }

    public function GuideDetails($id)
    {
        $data = $this->guide->getByID($id);
        return view('admin/detail',['data'=>$data]);
    }



}
