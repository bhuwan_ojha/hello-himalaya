<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body>
<section class="body">
	<?php echo $__env->make('admin.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="inner-wrapper">
		<?php echo $__env->make('admin.includes.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Guides</h2>					
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.php"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Listing</span></li>
							<li><span>Guides</span></li>
						</ol>
						<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>
				<section class="panel">
					<div class="panel-body">
						<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
							<thead>
								<tr>
									<th>Name</th>
									<th>Category</th>
									<th>Status</th>
									<th>Update/Delete</th>
								</tr>
							</thead>
							<tbody>

							<?php foreach($data as $value): ?>
								<tr>
									<td><?php echo e($value->business_name); ?></td>
									<td><?php echo e($value->business_category); ?></td>
									<td>
										<input type="checkbox" id="<?php echo e($value->ID); ?>" <?php if($value->status == 1): ?> checked <?php endif; ?> onchange="publishStatus(this)" data-toggle="toggle" data-on="Published" data-off="Unpublished">
										<script>
											$(function() {
												$('#<?php echo e($value->ID); ?>').bootstrapToggle({
													on: 'Published',
													off: 'Unpublished'
												});
											})
										</script>
									</td>
									<td>
										<div class="btn-group" role="group" aria-label="...">
											<button type="button" class="btn btn-sm btn-warning" onclick="location.href='<?php echo e(url('admin/hotel/saveupdate/'.$value->ID)); ?>'" title="update"><i class="fa fa-cogs"></i> Edit</button>
											<button type="button" class="btn btn-sm btn-danger" onclick="deleteHotel(<?php echo e($value->ID); ?>)" title="delete"><i class="fa fa-trash"></i> Delete</button>
										</div>
									</td>
								</tr>
								<?php endforeach; ?>

							</tbody>
						</table>
					</div>
				</section>
			</section>
		</div>
	</section>
	
	<div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add New</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						
					
						<div class="col-lg-12">
							<section class="panel">
								<div class="panel-body">
									<form class="form-horizontal form-bordered" method="get">
										<div class="form-group">
											<label class="col-md-3 control-label">Full Name</label>
											<div class="col-md-6">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Email</label>
											<div class="col-md-6">
												<input type="email" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Phone</label>
											<div class="col-md-6">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">URL</label>
											<div class="col-md-6">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Category</label>
											<div class="col-md-6">
												<input type="text" class="form-control">
											</div>
										</div>
									</form>
								</div>
							</section>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="addNew">
		<a href="<?php echo e(url('/admin/add-update-hotel')); ?>" ><i class="fa fa-plus"></i> Add New</a>
	</div>
<?php echo $__env->make('admin.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script>
    function deleteHotel(id){
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo e(url('admin/delete-hotel')); ?>',
            method:'post',
            data:{id:id},
            success:function () {
              location.reload();

            }
        })

	}
	
	function publishStatus(thisObj) {
        if($(thisObj).is(':checked')){
            var data = 1;
		}else if($(thisObj).not(':checked')){
            var data = 0;
		}
		var id = $(thisObj).attr('id');
		$.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo e(url('admin/hotel/update-status')); ?>',
            method: 'post',
            data: {status: data,id:id},
            success: function () {
                /*location.reload();*/
            }
        })
		
    }
</script>
</body>
</html>