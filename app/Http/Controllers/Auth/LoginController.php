<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback()
    {
        $providerUser = Socialite::driver('facebook')->user();
        $fbUserData = array(
            'username' => $providerUser['name'],
            'email' => $providerUser['email'],
            'user-id' => $providerUser['id'],
            'image' => $providerUser->avatar_original
        );

        $result = DB::table('tbl_fb_login')->insert($fbUserData);
        if($result){
           Session::set('facebookUserID', $providerUser['id']);
        }

        return redirect('hotel-details');
    }

}
