<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Hotel extends Model
{

    public function FindAll()
    {
        $data = DB::table('tbl_business')->get();
        return $data;
    }

    public function SaveUpdate($business_category, $guide_category, $id = null)
    {
        if ($id == 0) {
            if ($business_category != 0) {
                foreach ($business_category['business_category'] as $data_key => $data_value) {

                    $new_business_category_data = array(
                        'business_category' => $business_category['business_category'][$data_key],

                    );
                    DB::table('category')->insert($new_business_category_data);
                }

            }
            if ($guide_category != 0) {
                foreach ($guide_category['guide_category'] as $data_key => $data_value) {

                    $new_guide_category_data = array(
                        'guide_category' => $guide_category['guide_category'][$data_key],


                    );

                    DB::table('category')->insert($new_guide_category_data);


                }
            }
        }
    }

    public function get_categories()
    {
        $categories = DB::table('category')->distinct()->get();
        return $categories;
    }

    public function get_bussiness_categories()
    {
        $categories = DB::table('category')->select('business_category')->distinct()->orWhereNotNull('business_category')->get();
        return $categories;
    }

    public function addAmenities($data)
    {
       // dd($data);
       // foreach ($data['business_category'] as $data_key => $data_value) {
            foreach ($data['amenities'] as $amenity_key => $amenity_value) {

                foreach($data['amenities'][$amenity_key] as $sub_key=>$sub_value) {
                    $new_business_category = array(
                        'business_category' => $data['business_category'][$amenity_key],
                        'amenities' => $sub_value
                    );


                    DB::table('business_amenities')->insert($new_business_category);

                }



            }

        }

    public function get_distinct_amenities()
    {
        $amenities = DB::table('business_amenities')->select('amenities')->distinct()->orWhereNotNull('amenities')->get();
        return $amenities;
    }

    public function get_amneties_byID($id)
    {
        $amenities = DB::table('selected_amenities')->select('amenities')->where('business_id',$id)->get();
        return $amenities;
    }

    public function SaveUpdateBussiness($hotel_data,$hours,$business_gallery,$amenities,$id=null)
    {
        if ($id == 0) {
            DB::table('tbl_business')->insert($hotel_data);
            $id['hotel_id'] = DB::getPdo()->lastInsertId();
            $business_hours = array_merge($hours, $id);
            DB::table('business_hours')->insert($business_hours);
            foreach ($business_gallery as  $data_value) {
                $all_business_images = array(
                    'business_image' => $data_value['new_name'],
                    'image_title' => $data_value['image_title'],
                    'image_description' => $data_value['image_description'],
                    'created_date' => getCurrentDate(),
                    "business_id" => $id['hotel_id']
                );


                DB::table('business_gallery')->insert($all_business_images);

            }
            foreach ($amenities as  $data_value) {
                $selected_amenities = array(
                    'amenities' => $data_value,
                    'business_id' => $id['hotel_id'],
                );
                DB::table('selected_amenities')->insert($selected_amenities);
            }


        }else{
            $result = DB::table('tbl_business')->where('ID', $id)->delete();
            DB::table('tbl_business')->insert($hotel_data);
            $businessid = DB::getPdo()->lastInsertId();
            $business_id['id'] = $id;
            $business_hours = array_merge($hours, $business_id);
            $result = DB::table('business_hours')->where('ID', $id)->delete();
            DB::table('business_hours')->insert($business_hours);
            if (isset($business_gallery[0]['ID'])== null) {
                $result = DB::table('business_gallery')->where('ID', $id)->delete();
            }

            foreach ($business_gallery as  $data_value) {
                $all_business_images = array(
                    'business_image' => $data_value['new_name'],
                    'image_title' => $data_value['image_title'],
                    'image_description' => $data_value['image_description'],
                    'created_date' => getCurrentDate(),
                    "business_id" => $businessid
                );
                DB::table('business_gallery')->insert($all_business_images);

            }
            $result = DB::table('selected_amenities')->where('ID', $id)->delete();
            foreach ($amenities as  $data_value) {
                $selected_amenities = array(
                    'amenities' => $data_value,
                    'business_id' => $businessid,
                );
                DB::table('selected_amenities')->insert($selected_amenities);
            }




        }
    }

    public function getByID($id)
    {
        $business['business'] = DB::table('tbl_business')->where('ID', $id)->get();
        return $business;


    }

    public function get_businessImage($id)
    {

        $data = DB::table('business_gallery')->where('business_id',$id)->get();
        return $data;
    }

    public function get_business_hours($id)
    {
        $data = DB::table('business_hours')->where('hotel_id',$id)->get();
        return $data;
    }
}