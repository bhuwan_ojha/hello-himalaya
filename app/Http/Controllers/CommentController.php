<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    public function addComment(Request $request)
    {
        $data = array(
            'user-id'=>$request->input('user-id'),
            'message'=>$request->input('message'),
            'created_date'=>getCurrentDate(),
        );
        $result = DB::table('tbl_comment')->insert($data);
        return redirect('/hotel-details');
    }
}
