<?php
/**
 * Created by PhpStorm.
 * User: Nepsrock
 * Date: 2017-05-25
 * Time: 12:26 AM
 */

namespace App\Http\Controllers;
use App\Comment;
use App\Guides;
use App\Http\Controllers\Auth;
use Illuminate\Http\Request;


class PagesController extends Controller
{
    public function __construct()
    {
        $this->comment = new Comment();
        $this->guide = new Guides();
    }

    function index()
    {
        return view('index');
    }

    function travelGuide()
    {
        return view('guide');
    }

    function featuredList()
    {
        return view('list');
    }

    function recentList()
    {
        return view('list');
    }

    function popularList()
    {
        return view('list');
    }

    function hotelDetails(Request $request)
    {
        $session = $request->session()->get('facebookUserID');
        $comment = $this->comment->get_comment();

        return view('detail',['session'=>$session],['comment'=>$comment]);
    }



}