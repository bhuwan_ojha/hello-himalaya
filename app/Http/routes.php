<?php

/* admin routes */
Route::get('/admin', 'admin\LoginController@index');
Route::post('/admin/auth', 'admin\LoginController@Authenticate');
Route::get('/admin/pages', 'admin\PageController@index');
Route::get('/admin/dashboard', 'admin\PageController@dashboard');
Route::get('/admin/logout', 'admin\LoginController@logout');

Route::get('/admin/guide-add-update/', 'admin\PageController@Guide');
Route::get('/admin/guide-add-update/{id}', 'admin\GuideController@getById');
Route::post('/admin/guide-save-update', 'admin\GuideController@SaveUpdate');
Route::post('/admin/delete-achievement', 'admin\GuideController@deleteGuideAchievement');
Route::post('/admin/delete-gallery-image', 'admin\GuideController@deleteGalleryImage');
Route::post('/admin/delete-guide', 'admin\GuideController@DeleteGuide');
Route::post('/admin/update-publish-status', 'admin\GuideController@UpdatePublishStatus');
Route::get('/admin/guide-list', 'admin\GuideController@ListGuide');


Route::get('/admin/add-update-hotel', 'admin\HotelController@addHotel');
Route::post('/admin/hotel/saveupdate', 'admin\HotelController@SaveUpdate');
Route::get('/admin/hotel/saveupdate/{id}', 'admin\HotelController@getById');
Route::get('/admin/amenities', 'admin\HotelController@Amenities');
Route::post('/admin/add-amenities', 'admin\HotelController@AddAmenities');
Route::get('/admin/manage-category', 'admin\HotelController@ManageCategory');
Route::post('/admin/add-category', 'admin\HotelController@AddCategory');
Route::get('/admin/hotel-list', 'admin\HotelController@ListHotel');
Route::post('/admin/delete-hotel', 'admin\HotelController@DeleteHotel');
Route::post('/admin/hotel/update-status', 'admin\HotelController@UpdatePublishStatus');





/* frontend routes */
Route::get('/', 'PagesController@index');
Route::get('/travel-guide', 'PagesController@travelGuide');
Route::get('/featured-list', 'PagesController@featuredList');
Route::get('/recent-list', 'PagesController@recentList');
Route::get('/popular-list', 'PagesController@popularList');
Route::get('/hotel-details', 'PagesController@hotelDetails');
Route::get('/guide-details', 'GuideController@guideDetails');
Route::get('/guide-details/{id}', 'GuideController@GuideDetails');
Route::post('/comment-submit', 'CommentController@addComment');
Route::get('/guide-list', 'GuideController@ListGuides');


/*facebook */
Route::get('login/facebook', 'Auth\LoginController@redirect');
Route::get('login/facebook/callback', 'Auth\LoginController@callback');