<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<title><?php if(isset($pgName) && is_string($pgName)){echo $pgName;}else{echo 'Hello Himalayan Homes';} ?></title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&subset=latin,latin-ext" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
	<link rel="stylesheet" href="<?php echo e(url('public/assets/css/font-awesome.min.css')); ?>" type="text/css">
	<link rel="stylesheet" href="<?php echo e(url('public/assets/css/owl.carousel.css')); ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo e(url('public/assets/css/markercluster.css')); ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo e(url('public/assets/css/style.css')); ?>" type="text/css" id="css-primary">
    <link rel="stylesheet" href="<?php echo e(url('public/assets/css/style1.css')); ?>" type="text/css" id="css-primary">
    <script src="https://use.fontawesome.com/75f381fc0a.js"></script>
</head>
<body class="">