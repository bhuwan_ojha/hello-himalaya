
	<?php ($pgName = 'Climber Detail | Hello Himalayn Homes'); ?>
	<?php echo $__env->make('includes.top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=784260374946984";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
	<div class="page-wrapper">
		<div class="header-wrapper">
			<div class="header">
				<div class="header-inner">
					<?php echo $__env->make('includes/nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<?php echo $__env->make('includes/nav1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
			</div>
		</div>
		<div class="main-wrapper">
			<div class="main">
				<div class="main-inner">
					<div class="content">
						<div class="content-title">
							<div class="container">
								<h1>Ang Babu Sherpa</h1>
								<ul class="breadcrumb">
									<li><a href="<?php echo e(url('/')); ?>">Home</a> <i class="md-icon">keyboard_arrow_right</i></li>
									<li><a href="<?php echo e(url('/recent-list')); ?>">Listing</a> <i class="md-icon">keyboard_arrow_right</i></li>
									<li class="active">Profle</li>
								</ul>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-8 col-lg-9">
									<div class="push-top-bottom">
										<div class="listing-detail">
											<div class="gallery">
												<?php (dd($data)); ?>
												<?php foreach($guideimage as $guide): ?>
												<div class="gallery-item" style="background-image: url(<?php echo e(url('uploads'.$guide->images)); ?>);">
													<div class="gallery-item-description">
														Quisque ornare efficitur risus eu bibendum
													</div>
												</div>
												<?php endforeach; ?>
											</div>
											<h2>About</h2>
											<?php foreach($guide as $value): ?>
											<p><?php echo e($value->guide_about); ?></p>
											<?php endforeach; ?>
											<h2>Climbing Highlights</h2>
											<ul>
												<li>In July of 2001, became the first personto summit Mt.K2 twice within 12 months</li>
												<li>Summited 100 of the world's highest 14 peaks</li>
												<li>Made 17 sumits of 8,000m peaks</li>
											</ul>
											<h2>Message for the youth</h2>
											<p>Duis suscipit turpis suscipit lectus ultricies, placerat fermentum orci fringilla. Mauris semper magna cursus diam suscipit consectetur.</p>
											<h2>Message for international climbers</h2>
											<p>Integer condimentum sodales metus, quis pellentesque nisi tempor at. Quisque ornare efficitur risus eu bibendum. Donec feugiat consequat magna in interdum. Donec malesuada orci et mi fermentum, ac tincidunt arcu tincidunt. Ut fringilla tellus sem, quis rhoncus dolor condimentum id. Vivamus ut commodo felis. Fusce in lacus egestas, mattis ex et, molestie est.</p>
											<h2>Success Timeline</h2>
											<div class="container">
												<div class="faq">
													<div class="faq-item" data-content="2010">
														<div class="faq-item-question">
															<h2>Korean Expedition of Mt.Makalu</h2>
														</div>
														<div class="faq-item-answer">
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla blandit eu eros nec ultrices. Aliquam gravida dictum odio sed gravida. Nunc posuere imperdiet lectus, et rutrum arcu bibendum eu. Praesent vitae purus vulputate, mattis dolor non, euismod urna. Vivamus porta et urna ultricies commodo.</p>
														</div>
													</div>
													<div class="faq-item" data-content="2012">
														<div class="faq-item-question">
															<h2>Kporean expedition of Mt.Lhotse</h2>
														</div>
														<div class="faq-item-answer">
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla blandit eu eros nec ultrices. Aliquam gravida dictum odio sed gravida. Nunc posuere imperdiet lectus, et rutrum arcu bibendum eu. Praesent vitae purus vulputate, mattis dolor non, euismod urna. Vivamus porta et urna ultricies commodo.</p>
														</div>
													</div>
													<div class="faq-item" data-content="2014">
														<div class="faq-item-question">
															<h2>Ice Climbing Competition</h2>
														</div>
														<div class="faq-item-answer">
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla blandit eu eros nec ultrices. Aliquam gravida dictum odio sed gravida. Nunc posuere imperdiet lectus, et rutrum arcu bibendum eu. Praesent vitae purus vulputate, mattis dolor non, euismod urna. Vivamus porta et urna ultricies commodo.</p>
														</div>
													</div>
												</div>
											</div>
											<h2>3 comments in this topic</h2>
											<ul class="comments">
												<li>
													<div class="comment">			
														<div class="comment-author">
															<a href="#" style="background-image: url('public/assets/img/tmp/profile-1.jpg');"></a>
														</div>
														<div class="comment-content">			
															<div class="comment-meta">
																<div class="comment-meta-author">
																	Posted by <a href="#">admin</a>
																</div>
																<div class="comment-meta-reply">
																	<a href="#">Reply</a>
																</div>
																<div class="comment-meta-date">
																	<span>8:54 PM 11/23/2016</span>
																</div>
															</div>
															<div class="comment-body">
																<div class="comment-rating">
																	<i class="md-icon">star</i>
																	<i class="md-icon">star</i>
																	<i class="md-icon">star</i>
																	<i class="md-icon">star</i>
																	<i class="md-icon">star</i>
																</div>
																Integer condimentum sodales metus, quis pellentesque nisi tempor at. Quisque ornare efficitur risus eu bibendum. Donec feugiat consequat magna in interdum. Donec malesuada orci et mi fermentum
															</div>
														</div>
													</div>		
													<ul>
														<li>
															<div class="comment">			
																<div class="comment-author">
																	<a href="#" style="background-image: url('public/assets/img/tmp/profile-2.jpg');"></a>
																</div>
																<div class="comment-content">			
																	<div class="comment-meta">
																		<div class="comment-meta-author">
																			Posted by <a href="#">admin</a>
																		</div>
																		<div class="comment-meta-reply">
																			<a href="#">Reply</a>
																		</div>
																		<div class="comment-meta-date">
																			<span>8:54 PM 11/23/2016</span>
																		</div>
																	</div>
																	<div class="comment-body">
																		<div class="comment-rating">
																			<i class="fa fa-star"></i>
																			<i class="fa fa-star"></i>
																			<i class="fa fa-star"></i>
																			<i class="fa fa-star-half-o"></i>
																			<i class="fa fa-star-o"></i>
																		</div>
																		Donec malesuada orci et mi fermentum, ac tincidunt arcu tincidunt. Ut fringilla tellus sem, quis rhoncus dolor condimentum id. Vivamus ut commodo felis. Fusce in lacus egestas, mattis ex et, molestie est.
																	</div>
																</div>
															</div>									
														</li>
													</ul>
												</li>
												<li>
													<div class="comment">			
														<div class="comment-author">
															<a href="#" style="background-image: url('public/assets/img/tmp/profile-3.jpg');"></a>
														</div>
														<div class="comment-content">			
															<div class="comment-meta">
																<div class="comment-meta-author">
																	Posted by <a href="#">admin</a>
																</div>
																<div class="comment-meta-reply">
																	<a href="#">Reply</a>
																</div>
																<div class="comment-meta-date">
																	<span>8:54 PM 11/23/2016</span>
																</div>
															</div>
															<div class="comment-body">
																<div class="comment-rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-half-o"></i>
																</div>
																Integer condimentum sodales metus, quis pellentesque nisi tempor at. Quisque ornare efficitur risus eu bibendum. Donec feugiat consequat magna in interdum. Donec malesuada orci et mi fermentum
															</div>
														</div>
													</div>		
												</li>	
											</ul>
											<h2>Write Feedback</h2>
											<div class="comment-create clearfix">
												<form method="post" action="?">
													<div class="form-group">
														<label>Message</label>
														<textarea class="form-control" rows="5"></textarea>
													</div>
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group">
																<label>Name</label>
																<input type="text" class="form-control">
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>E-mail</label>
																<input type="email" class="form-control">
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label>URL</label>
																<input type="text" class="form-control">
															</div>				
														</div>
													</div>
													<div class="form-group-btn">
														<a href="#" class="btn btn-primary btn-large pull-right">Post Comment</a>
													</div>
												</form>
											</div>											
										</div>
									</div>
								</div>
								<div class="col-md-4 col-lg-3">
									<div class="sidebar">
										<div class="widget">
											<h2 class="widgettitle">Contact Information</h2>
											<ul class="contact">
												<li><i class="md-icon">email</i> <a href="mailto:sherpaang12@gmail.com">sherpaang12@gmail.com</a></li>
												<li><i class="md-icon">link</i> <a href="#">example.com</a></li>
												<li><i class="md-icon">phone</i> +01-23-456-789</li>
												<li><i class="md-icon">location_on</i> Some Street<br>Lukla, Solukhumbu</li>
											</ul>
										</div>
										<div class="widget social-fb">
											<div class="fb-page" data-href="https://www.facebook.com/HelloHimalayanHomes" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/HelloHimalayanHomes" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/HelloHimalayanHomes">Hello Himalayan Homes</a></blockquote></div>
										</div>										
										<div class="widget widget-background-white">
											<h3 class="widgettitle">Inquire Form</h3>
											<form method="post" action="?">
												<div class="form-group">
													<label>Full Name</label>
													<input type="text" class="form-control">
												</div>
												<div class="form-group">
													<label>E-mail</label>
													<input type="email" class="form-control">
												</div>
												<div class="form-group">
													<label>Subject</label>
													<input type="text" class="form-control">
												</div>             
												<div class="form-group">
													<label>Message</label>
													<textarea class="form-control" rows="4"></textarea>
												</div>                              
												<div class="form-group-btn">
													<button type="submit" class="btn btn-primary btn-block btn-large">Send Message</button>
												</div>
											</form>
										</div>
										<div class="widget">
										<h2 class="widgettitle">Recent Listings</h2>
										<div class="cards-small-wrapper">
											<div class="card-small">
												<div class="card-small-image">
													<a href="#" style="background-image: url('public/assets/img/user/2.jpg');"></a>
												</div>
												<div class="card-small-content">
													<h3><a href="#">Cozzy Coffee Shop</a></h3>
													<h4><a href="#">Drink &amp; Food</a></h4>
												</div>
											</div>
											<div class="card-small">
												<div class="card-small-image">
													<a href="#" style="background-image: url('public/assets/img/user/3.jpg');"></a>
												</div>
												<div class="card-small-content">
													<h3><a href="#">Cozzy Coffee Shop</a></h3>
													<h4><a href="#">Drink &amp; Food</a></h4>
												</div>
											</div>
											<div class="card-small">
												<div class="card-small-image">
													<a href="#" style="background-image: url('public/assets/img/user/4.jpg');"></a>
												</div>
												<div class="card-small-content">
													<h3><a href="#">Cozzy Coffee Shop</a></h3>
													<h4><a href="#">Drink &amp; Food</a></h4>
												</div>
											</div>
											<div class="card-small">
												<div class="card-small-image">
													<a href="#" style="background-image: url('public/assets/img/user/5.jpg');"></a>
												</div>
												<div class="card-small-content">
													<h3><a href="#">Cozzy Coffee Shop</a></h3>
													<h4><a href="#">Drink &amp; Food</a></h4>
												</div>
											</div>
										</div>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php echo $__env->make('includes/footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
<?php echo $__env->make('includes/btm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>