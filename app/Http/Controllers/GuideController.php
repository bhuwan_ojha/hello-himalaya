<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Guides;
use App\Comment;
use Illuminate\Support\Facades\DB;

class GuideController extends Controller
{

    public function __construct()
    {
        $this->comment = new Comment();
        $this->guide = new Guides();
    }
    function ListGuides()
    {
        $guide = $this->guide->FindAll();
        return view('guide-list',['guide'=>$guide]);
    }

    function GuideDetails($id)
    {
        $data = $this->guide->getByID($id);

        return view('guide-detail',['data'=>$data]);
    }


}
