	<script type="text/javascript" src="<?php echo e(url('public/assets/js/jquery.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('public/assets/js/tether.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('public/assets/js/bootstrap.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('public/assets/js/leaflet.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('public/assets/js/leaflet.markercluster.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('public/assets/libraries/owl-carousel/owl.carousel.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('public/assets/js/materialist.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('public/assets/js/auto-complete.min.js')); ?>"></script>
	<script>
		var demo1 = new autoComplete({
			selector: '#keywords',
			minChars: 1,
			source: function(term, suggest){
				term = term.toLowerCase();
				var choices = ['Hotels', 'Restaurants', 'Lodges', 'Home Stay', 'Guides', 'Climbers', 'Hospitals', 'Coffee Shop', 'Local Guide', 'Climbing Equipments', 'Electronics', 'Repair Shop', 'Rentsls', 'Vehicles', 'Hire Vehicle', 'Helicopter', 'Medical', 'Doctors', 'Local Food'];
				var suggestions = [];
				for (i=0;i<choices.length;i++)
				if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
				suggest(suggestions);
			}
		});
	</script>
</body>
</html>