@include('admin.includes.header')

<body>
@include('admin.includes.footer')
<section class="body">

	<div class="inner-wrapper">
		@include('admin.includes.nav')
			{{--<aside class="sidebar-left" id="sidebar-left">
				<div class="sidebar-header">
					<div class="sidebar-title">
						MENU
					</div>
					<div class="sidebar-toggle hidden-xs" data-fire-event="sidebar-left-toggle" data-target="html" data-toggle-class="sidebar-left-collapsed">
						<i aria-label="Toggle sidebar" class="fa fa-bars"></i>
					</div>
				</div>
				<div class="nano">
					<div class="nano-content">
						<nav class="nav-main" id="menu" role="navigation">
							<ul class="nav nav-main">
								<li class="nav-active">
									<a href="#"><i aria-hidden="true" class="fa fa-home"></i> <span>Dashboard</span></a>
								</li>
								<li>
									<a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-envelope"></i> <span>Messages</span></a>
								</li>
								<li>
									<a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-users"></i> <span>Contacts</span></a>
								</li>
								<li class="nav-parent">
									<a><i aria-hidden="true" class="fa fa-columns"></i> <span>Manage Listing</span></a>
									<ul class="nav nav-children">
										<li><a href="#">Guides</a></li>
										<li><a href="#">Business</a></li>
									</ul>
								</li>
								<li>
									<a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-users"></i> <span>Manage pages</span></a>
								</li>
								<li>
									<a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-users"></i> <span>calendar</span></a>
								</li>
								<li>
									<a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-users"></i> <span>Manage Comments</span></a>
								</li>
								<li class="nav-parent">
									<a><i aria-hidden="true" class="fa fa-columns"></i> <span>Account Setting</span></a>
									<ul class="nav nav-children">
										<li><a href="#">Profile</a></li>
										<li><a href="#">Users</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
					<script>
					// Maintain Scroll Position
						if (typeof localStorage !== 'undefined') {
							if (localStorage.getItem('sidebar-left-position') !== null) {
								var initialPosition = localStorage.getItem('sidebar-left-position'),
								sidebarLeft = document.querySelector('#sidebar-left .nano-content');
								sidebarLeft.scrollTop = initialPosition;
							}
						}
					</script>
				</div>
			</aside>--}}
			<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Guides</h2>					
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.php"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Listing</span></li>
							<li><span>Guides</span></li>
						</ol>
						<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>
				<section class="panel">
					<div class="panel-body">
						<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
							<thead>
								<tr>
									<th>Name</th>
									<th>Category</th>
									<th>Status</th>
									<th>Date Added</th>
									<th>Update/Delete</th>
								</tr>
							</thead>
							<tbody>

							@foreach ($data as $value)
								<tr>
									<td>{{$value->guide_name}}</td>
									<td>{{$value->guide_category}}</td>
									<td>
										<input type="checkbox" id="{{$value->ID}}" @if($value->status == 1) checked @endif onchange="publishStatus(this)" data-toggle="toggle" data-on="Published" data-off="Unpublished">
										<script>
											$(function() {
												$('#{{$value->ID}}').bootstrapToggle({
													on: 'Published',
													off: 'Unpublished'
												});
											})
										</script>
									</td>
									<td>{{$value->created_date}}</td>
									<td>
										<div class="btn-group" role="group" aria-label="...">
											<button type="button" class="btn btn-sm btn-warning" onclick="location.href='{{url('admin/guide-add-update/'.$value->ID)}}'" title="update"><i class="fa fa-cogs"></i> Edit</button>
											<button type="button" class="btn btn-sm btn-danger" onclick="deleteGuide({{$value->ID}})" title="delete"><i class="fa fa-trash"></i> Delete</button>
										</div>
									</td>
								</tr>
								@endforeach

							</tbody>
						</table>
					</div>
				</section>
			</section>
		</div>
	</section>
	
	<div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add New</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						
					
						<div class="col-lg-12">
							<section class="panel">
								<div class="panel-body">
									<form class="form-horizontal form-bordered" method="get">
										<div class="form-group">
											<label class="col-md-3 control-label">Full Name</label>
											<div class="col-md-6">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Email</label>
											<div class="col-md-6">
												<input type="email" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Phone</label>
											<div class="col-md-6">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">URL</label>
											<div class="col-md-6">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Category</label>
											<div class="col-md-6">
												<input type="text" class="form-control">
											</div>
										</div>
									</form>
								</div>
							</section>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="addNew">
		<a href="{{url('/admin/guide-add-update')}}" ><i class="fa fa-plus"></i> Add New</a>
	</div>
@include('admin.includes.footer')
<script>
    function deleteGuide(id){
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{url('admin/delete-guide')}}',
            method:'post',
            data:{id:id},
            success:function () {
              location.reload();

            }
        })

	}
	
	function publishStatus(thisObj) {
        if($(thisObj).is(':checked')){
            var data = 1;
		}else if($(thisObj).not(':checked')){
            var data = 0;
		}
		var id = $(thisObj).attr('id');
		$.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{url('admin/update-publish-status')}}',
            method: 'post',
            data: {status: data,id:id},
            success: function () {
                /*location.reload();*/
            }
        })
		
    }
</script>
</body>
</html>