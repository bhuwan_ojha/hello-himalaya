<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Guides extends Model
{
    public $timestamps = false;

    public function FindAll()
    {
        $data = DB::table('tbl_guides')->get();
        return $data;
    }

    public function getByID($id)
    {
        $guide['guide'] = DB::table('tbl_guides')->where('ID', $id)->get();
        $guide_timeline['guide_timeline'] = DB::table('guide_timeline')->where('guide_id', $id)->get();
        $guideimage['guideimage'] = DB::table('guide_gallery')->where('guide_id', $id)->get();
        $data = array_merge_recursive($guide,$guide_timeline,$guideimage);
        return $data;


    }

    function mergeArrays($guide, $guide_timeline, $guideimage) {
        $result = array();

        foreach ( $guide as $key=>$value ) {
            $result[] = array( 'guide' => $value, 'guide_timeline' => $guide_timeline[$key], 'guideimage' => $guideimage[ $key ] );
        }

        return $result;
    }

    public function SaveUpdate($data,$timelineData,$gallery_image,$id=null)
    {
        if($id == 0) {

            DB::table('tbl_guides')->insert($data);
            $id = DB::getPdo()->lastInsertId();

            foreach($timelineData['timeline_title'] as $data_key=>$data_value) {
                $new_timeline_data = array(
                    'timeline_title' => $timelineData['timeline_title'][$data_key],
                    'timeline_description' => $timelineData['timeline_description'][$data_key],
                    'date_time' =>$timelineData['date_time'][$data_key],
                    'created_date' => getCurrentDate(),
                    "guide_id"=>$id
                );
                DB::table('guide_timeline')->insert($new_timeline_data);
            }

            foreach($gallery_image as $data_key=>$data_value) {

                $all_galery_images = array(
                    'guide_image' => $data_value['new_name'],
                    'image_title' => $data_value['image_title'],
                    'image_description' =>$data_value['image_description'],
                    'created_date' => getCurrentDate(),
                    "guide_id"=>$id
                );

                DB::table('guide_gallery')->insert($all_galery_images);
            }

        }else {
            $result = DB::table('tbl_guides')->where('ID', $id)->delete();
            DB::table('tbl_guides')->insert($data);
            $guideid = DB::getPdo()->lastInsertId();

            $result = DB::table('guide_timeline')->where('guide_ID', $id)->delete();

            foreach($timelineData['timeline_title'] as $data_key=>$data_value) {
                $new_timeline_data = array(
                    'timeline_title' => $timelineData['timeline_title'][$data_key],
                    'timeline_description' => $timelineData['timeline_description'][$data_key],
                    'date_time' =>$timelineData['date_time'][$data_key],
                    'created_date' => getCurrentDate(),
                    "guide_id"=>$guideid
                );
                DB::table('guide_timeline')->insert($new_timeline_data);
            }

            if (isset($gallery_image[0]['ID'])== null) {
                $result = DB::table('guide_gallery')->where('ID',isset($gallery_image[0]['ID']))->delete();
            }
            foreach($gallery_image as $data_key=>$data_value) {

                $all_galery_images = array(
                    'guide_image' => $data_value['new_name'],
                    'image_title' => $data_value['image_title'],
                    'image_description' =>$data_value['image_description'],
                    'created_date' => getCurrentDate(),
                    "guide_id"=>$guideid
                );
                DB::table('guide_gallery')->insert($all_galery_images);
            }
        }

        redirect('admin/guides-list');


    }


    public function DeleteGuide($id)
    {
        return Guides::destroy($id);
    }

    public function list_guide()
    {
        $guides = DB::table('tbl_guides')->distinct()->get();
        return $guides;

    }


}


