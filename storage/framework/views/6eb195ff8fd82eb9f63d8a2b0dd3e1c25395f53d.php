<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php ($add = 1); ?>
<?php if(isset($data)!=0): ?>
    <?php ($add = 0); ?>
<?php endif; ?>
<body>
	<section class="body">

		<div class="inner-wrapper">
<?php echo $__env->make('admin.includes.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Guides</h2>					
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Listing</span></li>
							<li><span>Guides</span></li>
						</ol>
						<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>
				<form method="post" action="<?php echo e(url('/admin/guide-save-update')); ?>" enctype="multipart/form-data">
					<section class="panel">
						<div class="row">
							<div class="col-md-4 col-lg-3">
								<section class="panel">
									<div class="panel-body">
										<img src="<?php echo e(!$add ? url('uploads/'.$data['guide'][0]->guide_pic ):''); ?>" class="proImg" alt="Profile picture">
										<input type="hidden" name="guide_pic" value="<?php echo e((!$add) ? $data['guide'][0]->guide_pic : ''); ?>">
										<div class="overlay">
											<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
											<input type="hidden" name="ID" value="<?php echo e(!$add ? $data['guide'][0]->ID : ''); ?>" >
											<label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
											<input type="file"  name="guide_pic" onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
										</div>
									</div>
								</section>
							</div>
							<div class="col-md-8 col-lg-9">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
									</div>
									<h2 class="panel-title">Personal Information</h2>
								</header>
								<div class="panel-body">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Full Name</label>
												<input type="text" name="guide_name" class="form-control" value="<?php echo e((!$add) ? $data['guide'][0]->guide_name : ''); ?>">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Email</label>
												<input type="text" name="guide_email" class="form-control" value="<?php echo e((!$add) ? $data['guide'][0]->guide_email : ''); ?>">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Phone</label>
												<input type="email" name="guide_phone" class="form-control" value="<?php echo e((!$add) ? $data['guide'][0]->guide_phone : ''); ?>">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">URL</label>
												<input type="url" name="guide_website_url" class="form-control" value="<?php echo e((!$add) ? $data['guide'][0]->guide_website_url : ''); ?>">
											</div>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</section>
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Other Information</h2>
						</header>
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Contact Address</label>
										<select name="guide_category" class="form-control">
											<option value="Guide">Guide</option>
											<option value="Climber">Climber</option>
											<option value="Porter">Porter</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">										
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Contact Address</label>
										<textarea name="guide_location" class="form-control"><?php echo e((!$add) ? $data['guide'][0]->guide_location : ''); ?></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">About</label>
										<textarea name="guide_about" class="form-control"><?php echo e((!$add) ? $data['guide'][0]->guide_about : ''); ?></textarea>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Manage Contents</h2>
						</header>
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Contents</label>
										<textarea name="guide_description" class="form-control" id="editor1">
											<?php echo e((!$add) ? $data['guide'][0]->guide_description : ''); ?>

										</textarea>
										<script>
											CKEDITOR.replace( 'editor1' );
										</script>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Manage Achievements</h2>
						</header>
						<div class="panel-body">
							<div class="row">
								<?php if($add==0): ?>
								<?php foreach($data['guide_timeline'] as $timelineData): ?>
								<div class="col-sm-12">
									<div class="form-group">
										<input type="hidden" name="timelineID" value="<?php echo e((!$add) ? $timelineData->ID : ''); ?>">
										<label class="control-label">Date</label>
										<input class="form-control" name="date_time[]" type="text" value="<?php echo e((!$add) ? $timelineData->date_time : ''); ?>">
									</div>
									<div class="form-group">
										<label class="control-label">Title</label>
										<input class="form-control" name="timeline_title[]" type="text" value="<?php echo e((!$add) ? $timelineData->timeline_title : ''); ?>">
									</div>
									<div class="form-group">
										<label class="control-label">Description</label>
										<textarea class="form-control" name="timeline_description[]"><?php echo e((!$add) ? $timelineData->timeline_description : ''); ?></textarea>
									</div><br>
									<button type="button" onclick="deleteAchievement(<?php echo e($timelineData->ID); ?>)" class="removeAchivement btn btn-danger"><i class="fa fa-trash"></i> Delete Achievement</button>
									<hr>
								</div>
								<?php endforeach; ?>
								<?php endif; ?>

							</div>
							<div class="row">
								<div class="col-sm-12">
									<button type="button" class="addNewAchivement btn btn-primary"><i class="fa fa-plus"></i> Add New Achievement</button>
								</div>
							</div>
						</div>
					</section>
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Gallery Image</h2>
						</header>
						<div class="panel-body">
							<?php if($add==0): ?>
								<?php if($data['guideimage']!==''): ?>
									<?php foreach($data['guideimage'] as $galleryImage): ?>
							<div class="row">
								<div class="col-sm-3">
									<img src="<?php echo e(url('uploads/'.$galleryImage->guide_image)); ?>" class="proImg" alt="Gallery Image">
									<div class="overlay">
										<label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
										<input type="file"  name="guide_image[]" value="<?php echo e((!$add && $galleryImage->guide_image!=="") ? $galleryImage->guide_image : ''); ?>"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
									</div>
								</div>
								<div class="col-sm-9">
									<div class="form-group">
										<label class="control-label">Image Title</label>
										<input type="hidden" name="imageID" value="<?php echo e((!$add) ? $galleryImage->ID : ''); ?>">
										<input type="text" name="image_title[]" class="form-control" value="<?php echo e((!$add) ? $galleryImage->image_title : ''); ?>">
									</div>
									<div class="form-group">
										<label class="control-label">Image Description</label>
										<input type="text" name="image_description[]"  class="form-control" value="<?php echo e((!$add) ? $galleryImage->image_description : ''); ?>">
									</div><br>
									<button type="button" onclick="deleteGalleryImage(<?php echo e($galleryImage->ID); ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> Delete Image</button><hr>
								</div>
							</div>
							<?php endforeach; ?>
							<?php endif; ?>
							<?php endif; ?>

							<div class="row">
								<div class="col-sm-12">
									<button type="button" class="addNewImage btn btn-primary"><i class="fa fa-plus"></i> Add New Image</button>
								</div>
							</div>
						</div>
					</section>
					<div class="addNew">
						<button class="" type="submit"><i class="fa fa-save"></i> Save</button>
					</div>
				</form>
			</section>
		</div>


	</section>
	
	
	<?php echo $__env->make('admin.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
<script>
    $('.addNewAchivement').click(function() {
        $(this).before('<div class="col-sm-12"><div class="form-group"><label class="control-label">Date</label><input class="form-control" name="date_time[]" type="text" ></div><div class="form-group"><label class="control-label">Title</label><input class="form-control" name="timeline_title[]" type="text" ></div><div class="form-group"><label class="control-label">Description</label><textarea name="timeline_description[]" class="form-control"></textarea></div><br><button type="button" class="removeAchivement btn btn-danger"><i class="fa fa-trash"></i> Delete Achievement</button><hr></div>');
        $('#whatever').append(structure);
    });
    $(document).on('click','.removeAchivement',function() {
        $(this).parent('div').remove();
    });
    $('.addNewImage').click(function() {
        debugger;
        $(this).before('<div class="row"><div class="col-sm-3"><img src="http://lorempixel.com/200/200/nature/" class="proImg" alt="Gallery Image"><div class="overlay"><label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label><input type="file" name="guide_image[]"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" ></div></div><div class="col-sm-9"><div class="form-group"><label class="control-label">Image Title</label><input type="text" name="image_title[]" class="form-control"></div><div class="form-group"><label class="control-label">Image Description</label><input type="text" name="image_description[]" class="form-control"></div><br><button type="button" class="removeImage btn btn-danger"><i class="fa fa-trash"></i> Delete Image</button><hr></div></div>');
        $('#whatever').append(structure);
    });
    $(document).on('click','.removeImage',function() {
        $(this).parent('div').parent('div').remove();
    });

    $('.image-input').change(function() {
        debugger;
        $(this).val($(this).val().replace("C:\\fakepath\\", ""));
    });





    function deleteAchievement(id) {
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo e(url('admin/delete-achievement')); ?>',
            method:'post',
            data:{id:id},
            success:function () {
                $(this).parent('div').remove();

            }
        })
    }

    function deleteGalleryImage(id) {
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo e(url('admin/delete-gallery-image')); ?>',
            method:'post',
            data:{id:id},
            success:function () {
                $(this).parent('div').remove();

            }
        })
    }

</script>
</html>