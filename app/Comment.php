<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Comment extends Model
{
    public function get_comment()
    {
        $data = DB::table('tbl_comment')
            ->join('tbl_fb_login', 'tbl_comment.user-id', '=', 'tbl_fb_login.user-id')
            ->get();
        return $data;
    }
}
