
	@php($pgName = 'Travel Guide | Hello Himalayn Homes')
	@include('includes.top')

<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=784260374946984";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
	<div class="page-wrapper">
		<div class="header-wrapper">
			<div class="header">
				<div class="header-inner">
					@include('includes/nav')
					@include('includes/nav1')
				</div>
			</div>
		</div>
		<div class="main-wrapper">
			<div class="main">
				<div class="main-inner">
					<div class="content">
						<div class="content-title">
							<div class="container">
								<h1>Travel Guide</h1>
								<ul class="breadcrumb">
									<li><a href="index.blade.php">Home</a> <i class="md-icon">keyboard_arrow_right</i></li>
									<li class="active">Travel Guide</li>
								</ul>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-4 col-lg-3">
									<div class="sidebar">								
										<div class="widget">
											<h2 class="widgettitle">Travel Guide</h2>
											<ul class="menu nav nav-stacked">
												<li class="nav-item">
													<a href="guide.blade.php" class="nav-link">About Nepal</a>
												</li>
												<li class="nav-item">
													<a href="guide.blade.php" class="nav-link">About Khumbu</a>
												</li>
												<li class="nav-item">
													<a href="guide.blade.php" class="nav-link">Tourist Info</a>
												</li>
												<li class="nav-item">
													<a href="guide.blade.php" class="nav-link">Places to Visit</a>
												</li>
												<li class="nav-item">
													<a href="guide.blade.php" class="nav-link">FAQs</a>
												</li>
											</ul>
										</div>
										<div class="widget">
											<h2 class="widgettitle">Featured Listings</h2>
											<div class="cards-small-wrapper">
												<div class="card-small">
													<div class="card-small-image">
														<a href="#" style="background-image: url('public/assets/img/user/2.jpg');"></a>
													</div>
													<div class="card-small-content">
														<h3><a href="detail.blade.php">Cozzy Coffee Shop</a></h3>
														<h4><a href="detail.blade.php">Drink &amp; Food</a></h4>
													</div>
												</div>
												<div class="card-small">
													<div class="card-small-image">
														<a href="detail.blade.php" style="background-image: url('public/assets/img/user/3.jpg');"></a>
													</div>
													<div class="card-small-content">
														<h3><a href="detail.blade.php">Cozzy Coffee Shop</a></h3>
														<h4><a href="detail.blade.php">Drink &amp; Food</a></h4>
													</div>
												</div>
												<div class="card-small">
													<div class="card-small-image">
														<a href="detail.blade.php" style="background-image: url('public/assets/img/user/4.jpg');"></a>
													</div>
													<div class="card-small-content">
														<h3><a href="detail.blade.php">Cozzy Coffee Shop</a></h3>
														<h4><a href="detail.blade.php">Drink &amp; Food</a></h4>
													</div>
												</div>
												<div class="card-small">
													<div class="card-small-image">
														<a href="detail.blade.php" style="background-image: url('public/assets/img/user/5.jpg');"></a>
													</div>
													<div class="card-small-content">
														<h3><a href="detail.blade.php">Cozzy Coffee Shop</a></h3>
														<h4><a href="detail.blade.php">Drink &amp; Food</a></h4>
													</div>
												</div>
											</div>
										</div>
										<div class="widget">
											<h2 class="widgettitle">Recent Listings</h2>
											<div class="cards-small-wrapper">
												<div class="card-small">
													<div class="card-small-image">
														<a href="detail.blade.php" style="background-image: url('public/assets/img/user/2.jpg');"></a>
													</div>
													<div class="card-small-content">
														<h3><a href="detail.blade.php">Cozzy Coffee Shop</a></h3>
														<h4><a href="detail.blade.php">Drink &amp; Food</a></h4>
													</div>
												</div>
												<div class="card-small">
													<div class="card-small-image">
														<a href="detail.blade.php" style="background-image: url('public/assets/img/user/3.jpg');"></a>
													</div>
													<div class="card-small-content">
														<h3><a href="detail.blade.php">Cozzy Coffee Shop</a></h3>
														<h4><a href="detail.blade.php">Drink &amp; Food</a></h4>
													</div>
												</div>
												<div class="card-small">
													<div class="card-small-image">
														<a href="detail.blade.php" style="background-image: url('public/assets/img/user/4.jpg');"></a>
													</div>
													<div class="card-small-content">
														<h3><a href="detail.blade.php">Cozzy Coffee Shop</a></h3>
														<h4><a href="detail.blade.php">Drink &amp; Food</a></h4>
													</div>
												</div>
												<div class="card-small">
													<div class="card-small-image">
														<a href="detail.blade.php" style="background-image: url('public/assets/img/user/5.jpg');"></a>
													</div>
													<div class="card-small-content">
														<h3><a href="detail.blade.php">Cozzy Coffee Shop</a></h3>
														<h4><a href="detail.blade.php">Drink &amp; Food</a></h4>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-8 col-lg-9">
									<div class="content">
										<div class="container push-top-bottom">
											<div class="post-detail">
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel diam fermentum, molestie nibh varius, dictum ipsum. Sed a velit facilisis, tempus est eget, iaculis metus. Fusce ut felis non risus ornare <a href="#">malesuada vel sed augue</a>. Sed sit amet est augue. Fusce non interdum magna, pulvinar dictum tellus.</p>
												<h3><img class="alignleft" src="public/assets/img/gallery/u1a.jpg" alt="" width="301" height="201">Aenean egestas porta tristique</h3>
												<p>In dictum velit tortor, sit amet commodo urna consectetur eget. Vivamus non nibh sapien. Praesent dui ante, cursus eget risus vel, pulvinar male suada nisi. Aenean pulvinar diam nisi, sit amet maximus diam accumsan sed. Aenean viverra maximus libero. Ut in risus orci.</p>
												<p>Nam ipsum lorem, bibendum vel bibendum ac, dictum non metus. Fusce dictum libero dui, <mark>non tempus dui imperdiet</mark> donec fermentum sapien velit, eu porta turpis scelerisque eget. Donec dictum nisl a venenatis finibus. Aliquam ut urna quis est tincidunt scelerisque. Integer eu viverra ante, vel auctor lectus. Vivamus id convallis nulla. Etiam at dictum enim.</p>
												<blockquote><p>Praesent dui ante, cursus eget risus vel, pulvinar malesuada nisi.<cite>Albert Einstein</cite></p></blockquote>
												<p>Etiam ut leo sodales, tincidunt sapien ut, eleifend dui. Nulla facilisi. Suspendisse potenti. Vestibulum efficitur mauris et libero accumsan sagittis. Nunc sed faucibus tortor. Pellentesque habitant morbi tristique senectus.</p>
												<h3>Proin tincidunt ornare pellentesque.</h3>
												<p><img class="alignright" src="public/assets/img/gallery/u1b.jpg" alt="" width="371" height="249">Praesent et feugiat quam. Sed congue velit et hendrerit sollicitudin. Pellentesque hendrerit at sem vel tristique. Fusce in rhoncus neque, nec tempus justo. Sed non faucibus ante.</p>
												<p>Nullam semper diam libero, quis eleifend est imperdiet a. Fusce nulla nibh, volutpat ut hendrerit vel, auctor in urna. Etiam tincidunt et felis a posuere. Etiam ac consectetur mauris, <a href="#">sit amet mattis justo</a> rhoncus egestas erat. Vivamus non nibh sapien. Praesent dui ante, cursus eget risus vel, pulvinar malesuada nisi. Aenean pulvinar diam nisi, sit amet maximus diam accumsan sed.&nbsp;Pellentesque hendrerit at sem vel tristique. Fusce in rhoncus neque, nec tempus justo.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('includes/footer')
	</div>
@include('includes/btm')