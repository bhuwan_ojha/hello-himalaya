<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'tbl_admin';
    protected $fillable = ['ID','username','password','admin_image','full_name','role',
        'status','created_date','updated_date'];

    public function AdminAuth($username,$password)
    {
        $sql = Admin::where('status','1')->where('username',$username)->where('password',$password)->first();
        return $sql;
    }

}
