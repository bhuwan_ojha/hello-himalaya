@include('admin.includes.header')
<body>
<section class="body">
	<div class="inner-wrapper">
		@include('admin.includes.nav')
			<aside class="sidebar-left" id="sidebar-left">
				<div class="sidebar-header">
					<div class="sidebar-title">
						MENU
					</div>
					<div class="sidebar-toggle hidden-xs" data-fire-event="sidebar-left-toggle" data-target="html" data-toggle-class="sidebar-left-collapsed">
						<i aria-label="Toggle sidebar" class="fa fa-bars"></i>
					</div>
				</div>
				<div class="nano">
					<div class="nano-content">
						<nav class="nav-main" id="menu" role="navigation">
							<ul class="nav nav-main">
								<li class="nav-active">
									<a href="#"><i aria-hidden="true" class="fa fa-home"></i> <span>Dashboard</span></a>
								</li>
								<li>
									<a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-envelope"></i> <span>Messages</span></a>
								</li>
								<li>
									<a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-users"></i> <span>Contacts</span></a>
								</li>
								<li class="nav-parent">
									<a><i aria-hidden="true" class="fa fa-columns"></i> <span>Manage Listing</span></a>
									<ul class="nav nav-children">
										<li><a href="#">Guides</a></li>
										<li><a href="#">Business</a></li>
									</ul>
								</li>
								<li>
									<a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-users"></i> <span>Manage pages</span></a>
								</li>
								<li>
									<a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-users"></i> <span>calendar</span></a>
								</li>
								<li>
									<a href="#"><span class="pull-right label label-primary">182</span> <i aria-hidden="true" class="fa fa-users"></i> <span>Manage Comments</span></a>
								</li>
								<li class="nav-parent">
									<a><i aria-hidden="true" class="fa fa-columns"></i> <span>Account Setting</span></a>
									<ul class="nav nav-children">
										<li><a href="#">Profile</a></li>
										<li><a href="#">Users</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
					<script>
					// Maintain Scroll Position
						if (typeof localStorage !== 'undefined') {
							if (localStorage.getItem('sidebar-left-position') !== null) {
								var initialPosition = localStorage.getItem('sidebar-left-position'),
								sidebarLeft = document.querySelector('#sidebar-left .nano-content');
								sidebarLeft.scrollTop = initialPosition;
							}
						}
					</script>
				</div>
			</aside>
			<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Guides</h2>					
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.php"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Listing</span></li>
							<li><span>Manage Amenities</span></li>
						</ol>
						<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>
				<form method="post" action="{{url('admin/add-amenities')}}" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Manage Amenities</h2>
						</header>
						<div class="panel-body">
							<div class="row">
								@foreach($data as $index=>$value)
								<div class="col-sm-12">
									<h3>{{$value->business_category}}</h3>
									<input type="hidden" name="business_category[]" value="{{$value->business_category}}">
									<select multiple data-role="tagsinput" name="amenities[{{$index}}][]" >

									</select>
								</div><hr>
									@endforeach
							</div>
						</div>
					</section>
					<div class="addNew">
						<button class="" type="submit"><i class="fa fa-save"></i> Save</button>
					</div>
				</form>
			</section>
		</div>
	</section>

@include('admin.includes.footer')
</body>
</html>