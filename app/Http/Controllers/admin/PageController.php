<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
   public function __construct()
   {
       /*$this->middleware('admin');*/
   }

   public function dashboard()
   {
       return view('admin/dashboard');
   }

   public function Guide()
   {
       return view('admin/detail');
   }
}
